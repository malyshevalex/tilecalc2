

import cm from '../utils/cm';

const AREA = 0;
const WINDOW = 1;
const DOOR = 2;

const CONSTRAINTS = {
    [DOOR]: {
        width: [0.5, undefined],
        height: [1.5, undefined]
    },
    [WINDOW]: {
        width: [0.3, 2],
        height: [0.3, 2]
    },
    [AREA]: {
        width: [0.1, undefined],
        height: [0.1, undefined]
    }
};


class WallObject {
    static AREA = AREA;
    static WINDOW = WINDOW;
    static DOOR = DOOR;

    static Types = [DOOR, WINDOW, AREA];

    static getLabel(type) {
        switch (type) {
            case WallObject.DOOR:
                return 'Дверь';
            case WallObject.WINDOW:
                return 'Окно';
            default:
                return 'Произвольная область';
        }
    }

    static getGenitive(type) {
        switch (type) {
            case WallObject.DOOR:
                return 'двери';
            case WallObject.WINDOW:
                return 'окна';
            default:
                return 'произвольной области';
        }
    }

    static getMinSize(type) {
        return {
            width: CONSTRAINTS[type].width[0],
            height: CONSTRAINTS[type].height[0]
        };
    }

    constructor(type, wallIndex, {left, bottom, width, height}) {
        this.type = type;
        this.wallIndex = wallIndex;
        if (this.type === DOOR) {
            this.bottom = 0;
        }
        this.left = left;
        this.bottom = bottom;
        this.width = width;
        this.height = height;
    }

    get right() {
        return cm.round(this.left + this.width);
    }

    get top() {
        return cm.round(this.bottom + this.height);
    }

    get constraints() {
        return CONSTRAINTS[this.type];
    }

    get isDoor() {
        return this.type === DOOR;
    }

    get isWindow() {
        return this.type === WINDOW;
    }

    get isValid() {
        let c = CONSTRAINTS[this.type];
        return ((isNaN(c.width[0]) || this.width >= c.width[0]) && (isNaN(c.height[0]) || this.height >= c.height[0])
                && (isNaN(c.width[1]) || this.width <= c.width[1]) && (isNaN(c.height[1]) || this.height <= c.height[1]));
    }

    isIntersectWith(other) {
        return !(this.right <= other.left || this.left >= other.right || this.top <= other.bottom || this.bottom >= other.top);
    }

    move(left, bottom) {
        if (this.type === DOOR) {
            bottom = 0;
        }
        this.left = Math.max(0, left);
        this.bottom = Math.max(0, bottom);
    }

    resize(width, height) {
        let c = this.constraints;
        this.width = Math.max(isNaN(c.width[0]) ? 0 : c.width[0], Math.min(isNaN(c.width[1]) ? Infinity : c.width[1], width));
        this.height = Math.max(isNaN(c.height[0]) ? 0 : c.height[0], Math.min(isNaN(c.height[1]) ? Infinity : c.height[1], height));
    }
}

export default WallObject;
