import normalizeAngle from '../utils/normalizeAngle';

const MAX_ANGLE = 5 * Math.PI / 6;

class Point {

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.leftWall = null;
        this.rightWall = null;
    }

    clone() {
        return new Point(this.x, this.y);
    }

    get angle() {
        if (this.leftWall && this.rightWall) {
            return normalizeAngle(this.rightWall.angle - this.leftWall.angle);
        } else {
            return 0;
        }
    }

    get next() {
        return this.rightWall ? this.rightWall.to : null;
    }

    get prev() {
        return this.leftWall ? this.leftWall.from : null;
    }

    get isValid() {
        return this.angle <= MAX_ANGLE;
    }

    moveTo(x, y) {
        this.x = x;
        this.y = y;
        if (this.leftWall) {
            this.leftWall.calculate();
        }
        if (this.rightWall) {
            this.rightWall.calculate();
        }
    }
}

export default Point;
