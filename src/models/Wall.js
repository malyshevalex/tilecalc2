import cm from '../utils/cm';
import normalizeAngle from '../utils/normalizeAngle';
import Bounds from './Bounds';

const MIN_LENGTH = 0.1;
const MAX_LENGTH = 10;

class Wall {

    /**
     * @param {Array} point
     * @param {Wall} [rightSibling]
     */
    constructor(point, rightSibling) {
        this._point = point;
        this._left = null;
        this._right = null;
        this._closed = false;
        this.objects = [];
        this.tiles = null;
        if (rightSibling) {
            this.cons(rightSibling);
        }
    }

    /**
     * Attach right tail to wall
     * @param {Wall} rightSibling
     */
    cons(rightSibling) {
        this._right = rightSibling;
        rightSibling._left = this;
        this._calculate();
        return this;
    }

    close(returnLast) {
        if (this.isClosed) {
            return this;
        }
        // try to find head
        let head = this;
        while (head.leftSibling) {
            head = head.leftSibling;
        }
        head._closed = true;
        let last = head;
        while (last.rightSibling) {
            last = last.rightSibling;
            last._closed = true;
        }
        last.cons(head);
        return returnLast ? last : head;
    }

    _calculate() {
        let dx = this.x2 - this.x1;
        let dy = this.y2 - this.y1;
        this.exactLength = Math.sqrt(dx * dx + dy * dy);
        this.length = cm.round(this.exactLength);
        this.angle = Math.atan2(dy, dx);
    }

    get x1() {
        return this._point[0];
    }

    get x2() {
        return this.rightSibling._point[0];
    }

    get y1() {
        return this._point[1];
    }

    get y2() {
        return this.rightSibling._point[1];
    }

    get leftSibling() {
        return this._left;
    }

    get rightSibling() {
        return this._right;
    }

    get leftAngle() {
        return this.leftSibling ? normalizeAngle(this.angle - this.leftSibling.angle) : 0;
    }

    get rightAngle() {
        return this.rightSibling ? normalizeAngle(this.rightSibling.angle - this.angle) : 0;
    }

    get isClosed() {
        return this._closed;
    }

    get isValid() {
        return this.length >= MIN_LENGTH && this.length <= MAX_LENGTH;
    }

    isIntersectsWith(other) {
        let ax1 = this.x1, ay1 = this.y1, ax2 = this.x2, ay2 = this.y2,
            bx1 = other.x1, by1 = other.y1, bx2 = other.x2, by2 = other.y2;
        let
            v1 = (bx2 - bx1) * (ay1 - by1) - (by2 - by1) * (ax1 - bx1),
            v2 = (bx2 - bx1) * (ay2 - by1) - (by2 - by1) * (ax2 - bx1),
            v3 = (ax2 - ax1) * (by1 - ay1) - (ay2 - ay1) * (bx1 - ax1),
            v4 = (ax2 - ax1) * (by2 - ay1) - (ay2 - ay1) * (bx2 - ax1);
        return ((v1 * v2 < 0) && (v3 * v4 < 0));
    }

    static getMaxLength() {
        return MAX_LENGTH;
    }

    static getMinLength() {
        return MIN_LENGTH;
    }

    static forEach(head, cb, thisArg) {
        if (!(head instanceof Wall)) {
            throw 'head should be a Wall Instance';
        }
        if (!head.isClosed) {
            head = Wall.findHead(head);
        }
        let cur = head;
        let counter = 0;
        do {
            cb.call(thisArg, cur, counter);
            counter++;
            cur = cur.rightSibling;
        } while (cur && cur !== head);
    }

    static transform(head, cb, options) {
        let isClosed = head.isClosed;
        let {inPlace, reverse} = options || {};
        if (reverse) {
            inPlace = false;
        }
        if (!isClosed) {
            head = Wall.findHead(head);
        }
        let cur = null, old = null;
        Wall.forEach(head, function(wall, i) {
            let newPoint = cb(wall, i);
            if (inPlace) {
                wall._point = newPoint;
                if (wall.leftSibling) {
                    wall.leftSibling._calculate();
                }
                cur = wall;
            } else {
                let newWall = new Wall(cb(wall, i));
                if (cur) {
                    let joined = reverse ? newWall.cons(cur) : cur.cons(newWall);
                    cloneInternals(joined, old);
                }
                old = wall;
                cur = newWall;
            }
        });
        if (isClosed) {
            if (inPlace) {
                cur._calculate();
            } else {
                cloneInternals(cur.close(true), old);
            }
        }
        let newHead = Wall.findHead(cur);
        let bounds = Wall.getWallsBounds(newHead);
        Wall.forEach(newHead, (w) => {
            w._point[0] -= bounds.x1;
            w._point[1] -= bounds.y1;
        });
        return newHead;
    }

    static transformMatrix(head, matrix, options) {
        return Wall.transform(head, (wall) => {
            return [
                wall.x1 * matrix[0] + wall.y1 * matrix[2] + matrix[4],
                wall.x1 * matrix[1] + wall.y1 * matrix[3] + matrix[5]
            ];
        }, options);
    }

    static count(wall) {
        let c = 0;
        Wall.forEach(wall, () => { c++; });
        return c;
    }

    static wallsToArray(head) {
        let arr = [];
        Wall.forEach(head, function(w) { arr.push(w); });
        return arr;
    }

    static wallsToPoints(head) {
        return Wall.wallsToArray(head).map(wall => [wall.x1, wall.y1]);
    }

    static getWallsBounds(head) {
        let points = Wall.wallsToPoints(head);
        let first = points.slice(1)[0];
        let bounds = new Bounds(first[0], first[1], first[0], first[1]);
        points.forEach(p => {
            bounds.x1 = Math.min(bounds.x1, p[0]);
            bounds.y1 = Math.min(bounds.y1, p[1]);
            bounds.x2 = Math.max(bounds.x2, p[0]);
            bounds.y2 = Math.max(bounds.y2, p[1]);
        });
        return bounds;
    }

    static findHead(wall) {
        if (wall.isClosed) {
            let head = wall;
            Wall.forEach(wall, function(w) {
                if (w.x1 < head.x1 || w.x1 === head.x1 && w.y1 < head.y1) {
                    head = w;
                }
            });
            return head;
        } else {
            while (wall.leftSibling) {
                wall = wall.leftSibling;
            }
            return wall;
        }
    }

    static resizeWall(wall, length) {
        let angle = wall.angle,
            dL = length - wall.length,
            from = [wall.x1, wall.y1], to = [wall.x2, wall.y2],
            cos = Math.cos(angle), sin = Math.sin(angle),
            sx = (cos === 0) ? 0 : Math.abs(cos) / cos,
            sy = (sin === 0) ? 0 : Math.abs(sin) / sin,
            dx = dL * cos, dy = dL * sin;

        return Wall.transform(wall, function(w) {
            let x = w.x1, y = w.y1;
            if (sx * (x - from[0]) > 0) {
                if (sx * (x - to[0]) >= 0) {
                    x += dx;
                } else {
                    x += dx * (x - from[0]) / (to[0] - from[0]);
                }
            }
            if (sy * (y - from[1]) > 0) {
                if (sy * (y - to[1]) >= 0) {
                    y += dy;
                } else {
                    y += dy * (y - from[1]) / (to[1] - from[1]);
                }
            }
            return [x, y];
        }, {
            inPlace: true
        });
    }

    static insertPoint(wall, x, y) {
        let newWall = new Wall([x, y], wall.rightSibling);
        wall.cons(newWall);
        if (wall.isClosed) {
            newWall._closed = true;
        }
        return Wall.findHead(wall);
    }

    static dropWall(wall) {
        let count = Wall.count(wall);
        if (count > 1 && (!wall.isClosed || count > 3)) {
            let left = wall.leftSibling, right = wall.rightSibling;
            if (left) {
                left._right = right;
            }
            if (right) {
                right._left = left;
                if (left) {
                    left.cons(right);
                }
            }
            wall = Wall.findHead(left || right);
        }
        return wall;
    }
}

function cloneInternals(target, source) {
    target.objects = source.objects.filter(o => (o.right <= target.length));
}

export default Wall;
