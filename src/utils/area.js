
export default function(poly) {
    let area = 0;
    let pointObject = poly[0].hasOwnProperty('x'),
        kx = pointObject ? 'x' : 0,
        ky = pointObject ? 'y' : 1;
    for (var i = 0, j = poly.length - 1; i < poly.length; j = i++) {
        area += poly[j][kx] * poly[i][ky] - poly[i][kx] * poly[j][ky];
    }
    return area / 2;
}
