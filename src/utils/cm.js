

export default {
    round: function(n) {
        return Math.round(n * 100) / 100;
    },
    ceil: function(n) {
        return Math.ceil(n * 100) / 100;
    },
    floor: function(n) {
        return Math.floor(n * 100) / 100;
    }
};
