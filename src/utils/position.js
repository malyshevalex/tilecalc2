
function offset(el) {
    return {
        x: el.offsetLeft,
        y: el.offsetTop
    };
}

export default {
    global(el) {
        let pos = offset(el), parent = el.offsetParent;
        while (parent) {
            pos.x += parent.offsetLeft - parent.scrollLeft;
            pos.y += parent.offsetTop - parent.scrollTop;
            el = parent;
            parent = el.offsetParent;
        }
        return pos;
    }
};
