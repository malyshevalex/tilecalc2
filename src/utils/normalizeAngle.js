
let PI = Math.PI,
    PI_2 = 2 * PI;

export default function(angle) {
    let sign = (angle >= 0) ? 1 : -1;
    while (Math.abs(angle) > PI_2) {
        angle += -PI_2 * sign;
    }
    if (angle > PI) {
        return angle - PI_2;
    } else if (angle < -PI) {
        return angle + PI_2;
    }
    return angle;
}
