
export default function float2str(value, unit) {
    value = value.toString().replace('.', ',');
    if (unit) {
        value += ' ' + unit;
    }
    return value;
}
