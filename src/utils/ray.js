
export function intersection(x1, y1, a, x2, y2, b) {
    let x, y;
    let ta = Math.tan(a);
    let tb = Math.tan(b);
    if (Math.abs(a) === Math.PI / 2) {
        x = x1;
        y = y2 + tb * (x - x2);
    } else {
        if (Math.abs(b) === Math.PI / 2) {
            x = x2;
        } else {
            x = (y1 - ta * x1 - y2 + tb * x2) / (tb - ta);
        }
        y = y1 + ta * (x - x1);
    }
    return {x, y};
}

