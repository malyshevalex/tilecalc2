import ActionTypes from '../constants/ActionTypes';
import Dispatcher from '../core/Dispatcher';


export default {
    createProject(points) {
        Dispatcher.dispatch({
            type: ActionTypes.CREATE_PROJECT,
            points
        });
    },

    movePoint(wall, dx, dy) {
        Dispatcher.dispatch({
            type: ActionTypes.MOVE_POINT,
            wall,
            dx,
            dy
        });
    },

    dropPoint(wall) {
        Dispatcher.dispatch({
            type: ActionTypes.DROP_POINT,
            wall
        });
    },

    insertPoint(wall, x, y) {
        Dispatcher.dispatch({
            type: ActionTypes.INSERT_POINT,
            wall,
            x,
            y
        });
    },

    resizeWall(wall, length) {
        Dispatcher.dispatch({
            type: ActionTypes.RESIZE_WALL,
            wall,
            length
        });
    },

    rotateScene() {
        Dispatcher.dispatch({
            type: ActionTypes.ROTATE_SCENE
        });
    },

    mirrorScene() {
        Dispatcher.dispatch({
            type: ActionTypes.MIRROR_SCENE
        });
    },

    setHeight(height) {
        Dispatcher.dispatch({
            type: ActionTypes.SET_HEIGHT,
            height
        });
    },

    addObjectToWall(wall, objectType) {
        Dispatcher.dispatch({
            type: ActionTypes.ADD_OBJECT_TO_WALL,
            wall, objectType
        });
    },

    saveWallObject(object) {
        Dispatcher.dispatch({
            type: ActionTypes.SAVE_WALL_OBJECT,
            object
        });
    },

    deleteWallObject(object) {
        Dispatcher.dispatch({
            type: ActionTypes.DELETE_WALL_OBJECT,
            object
        });
    },

    moveWallObject(object, left, bottom) {
        Dispatcher.dispatch({
            type: ActionTypes.MOVE_WALL_OBJECT,
            object, left, bottom
        });
    },

    resizeWallObject(object, width, height) {
        Dispatcher.dispatch({
            type: ActionTypes.RESIZE_WALL_OBJECT,
            object, width, height
        });
    },

    editWallObject(object) {
        Dispatcher.dispatch({
            type: ActionTypes.EDIT_WALL_OBJECT,
            object
        });
    },

    setSurfaceNeeded(surfaceType, state) {
        Dispatcher.dispatch({
            type: ActionTypes.SET_SURFACE_NEEDED,
            surfaceType,
            state
        });
    },
    setSurfaceCollection(surfaceType, manufacturer, collection) {
        Dispatcher.dispatch({
            type: ActionTypes.SET_SURFACE_COLLECTION,
            surfaceType,
            manufacturer,
            collection
        });
    },
    setSurfaceOption(surfaceType, options) {
        Dispatcher.dispatch({
            type: ActionTypes.SET_SURFACE_OPTIONS,
            surfaceType,
            options
        });
    }
};
