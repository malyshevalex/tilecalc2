import ActionTypes from '../constants/ActionTypes';
import Dispatcher from '../core/Dispatcher';


export default {
    showTooltip(x, y, text) {
        Dispatcher.dispatch({
            type: ActionTypes.SHOW_TOOLTIP,
            x, y, text
        });
    },

    hideTooltip() {
        Dispatcher.dispatch({
            type: ActionTypes.HIDE_TOOLTIP
        });
    },

    startDrag(node, event, areas) {
        Dispatcher.dispatch({
            type: ActionTypes.START_DRAG,
            node, event, areas
        });
    },

    checkDragIntersections(poly) {
        Dispatcher.dispatch({
            type: ActionTypes.CHECK_DRAG_INTERSECTIONS,
            poly
        });
    },

    changeView(view) {
        Dispatcher.dispatch({
            type: ActionTypes.CHANGE_VIEW,
            view
        });
    }

};
