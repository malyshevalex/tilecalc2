import React from 'react';
import classNames from 'classname';

import './RoomShape.styl';

const PropTypes = React.PropTypes;

class RoomShape extends React.Component {

    static propTypes = {
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        points: PropTypes.array.isRequired,
        onClick: PropTypes.func
    };

    static defaultProps = {
        width: 0,
        height: 0
    };

    componentWillMount() {
        this.calcRatio();
    }

    componentWillReceiveProps() {
        this.calcRatio();
    }

    calcRatio() {
        let maxX = 0;
        let maxY = 0;
        this.props.points.forEach(point => {
            if (maxX < point[0]) {
                maxX = point[0];
            }
            if (maxY < point[1]) {
                maxY = point[1];
            }
        });
        this.ratio = Math.min(this.props.width / maxX, this.props.height / maxY);
        this.viewBox = [-2, -2, maxX * this.ratio + 4, maxY * this.ratio + 4].join(' ');
    }

    onClick = (e) => {
        if (this.props.onClick) {
            this.props.onClick(e, this);
        }
    };

    render() {
        let points = Array.prototype.concat.apply([], this.props.points).map(c => c * this.ratio).join(',');
        let style = {
            width: this.props.width,
            height: this.props.height
        };
        let clickable = !!this.props.onClick;
        let className = classNames('RoomShape', {
            'RoomShape--clickable': clickable
        });
        return (
            <svg viewBox={this.viewBox} style={style} className={className} onClick={clickable ? this.onClick : null}>
                <polygon points={points} className="RoomShape-walls" />
            </svg>
        );
    }
}


export default RoomShape;
