import React from 'react';  //  eslint-disable-line no-unused-vars
import PureComponent from 'react-pure-render/component';
import ProjectStore from '../../stores/ProjectStore';


export default class extends PureComponent {

    componentDidMount() {
        ProjectStore.on(this.elementType + '.change', this.handleModelChange, this);
        ProjectStore.on('room.change', this.handleRoomChange, this);
    }

    componentWillUnmount() {
        ProjectStore.off(this.elementType + '.change', this.handleModelChange, this);
        ProjectStore.off('room.change', this.handleRoomChange, this);
    }

    componentWillMount() {
        this.assignModel(this.props.model);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.model !== this.props.model) {
            this.assignModel(nextProps.model);
        }
    }

    handleRoomChange() {
        this.handleModelChange(this.props.model);
    }

    handleModelChange(model) {
        if (model === this.props.model) {
            this.assignModel(model);
        }
    }
}
