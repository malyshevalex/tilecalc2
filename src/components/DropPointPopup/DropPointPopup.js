
import React from 'react';
import popup from '../../decorators/popup';

import Button from '../Button';


class DropPointPopup extends React.Component {

    static propTypes = {
        onDrop: React.PropTypes.func
    };

    handleClick = () => {
        if (this.props.onDrop) {
            this.props.onDrop();
        }
    };

    render() {
        return (
            <Button label="Удалить точку преломления" icon="delete" onClick={this.handleClick} />
        );
    }
}

export default popup(DropPointPopup);
