import React from 'react';
import Select from 'react-select';

import CollectionsStore from '../../stores/CollectionsStore';

import TooltipPoint from '../TooltipPoint';

import './CollectionSelect.styl';

function renderManufacturerOptionAndValue(option) {
    return <span><span className="CollectionSelect-optionFlag" style={{backgroundImage: `url(${option.country.flagUrl})`}} /> {option.label}</span>;
}

function renderCollectionOptionAndValue(option) {
    return <span><span className="CollectionSelect-optionType">{option.type}</span>{option.label}</span>;
}

const PropTypes = React.PropTypes;

class CollectionSelect extends React.Component {

    static propTypes = {
        type: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        titleTooltip: PropTypes.string,
        enableTitle: PropTypes.string.isRequired,
        enableTooltip: PropTypes.string,
        manufacturer: PropTypes.string,
        enabled: PropTypes.bool,
        onSelect: PropTypes.func,
        onChangeState: PropTypes.func
    };

    static defaultProps = {
        enabled: false
    };

    componentWillMount() {
        let manufacturer = this.props.manufacturer;
        this.setState({
            enabled: this.props.enabled,
            manufacturer,
            collection: this.props.collection
        });
        this.makeManufacturerOptions();
        this.makeCollectionOptions(manufacturer);
    }

    componentWillUnmount() {
        delete this.manufacturerOptions;
        delete this.collectionOptions;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            enabled: nextProps.enabled,
            manufacturer: nextProps.manufacturer,
            collection: nextProps.collection
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (this.state.enabled !== nextState.enabled
                || this.state.manufacturer !== nextState.manufacturer
                || this.state.collection !== nextState.collection);
    }

    componentWillUpdate(nextProps, nextState) {
        let currentManufacturer = this.state.manufacturer,
            nextManufacturer = nextState.manufacturer;
        if (currentManufacturer !== nextManufacturer) {
            this.makeCollectionOptions(nextManufacturer);
        }
    }

    makeManufacturerOptions() {
        this.manufacturerOptions = CollectionsStore.getManufacturers(this.props.type).map(m => {
            return {
                value: m.id,
                label: m.name,
                country: CollectionsStore.getCountry(m.country)
            };
        });
    }

    makeCollectionOptions(manufacturer) {
        if (manufacturer) {
            this.collectionOptions = CollectionsStore.getCollections(this.props.type, manufacturer).map(c => {
                return {
                    value: c.id,
                    label: c.name,
                    type: c.type ? CollectionsStore.getType(c.type).title : ''
                };
            });
        } else {
            this.collectionOptions = [];
        }
    }

    enabledChangeHandler = (e) => {
        let state = e.target.checked;
        this.setState({
            enabled: state
        });
        if (this.props.onChangeState) {
            this.props.onChangeState.call(null, state);
        }
    };

    handleManufacturerChange = (manufacturer) => {
        if (this.props.onSelect) {
            this.props.onSelect.call(null, manufacturer, null);
        }
    };

    handleCollectionChange = (collection) => {
        if (this.props.onSelect) {
            this.props.onSelect.call(null, this.state.manufacturer, collection);
        }
    };

    render() {
        return (
            <div className="CollectionSelect">
                <h2>{this.props.title} { this.props.titleTooltip ? <TooltipPoint tooltip={this.props.titleTooltip} /> : '' }</h2>
                <div className="CollectionSelect-check">
                    <label>
                        <input type="checkbox" defaultChecked={this.props.enabled} onChange={this.enabledChangeHandler} />
                        <span className="CollectionSelect-title">{this.props.enableTitle}&nbsp;</span>
                    </label>
                    { this.props.enableTooltip ? <TooltipPoint tooltip={this.props.enableTooltip} /> : '' }
                </div>

                <div className="CollectionSelect-select CollectionSelect-select--manufacturer">
                    <Select
                        options={this.manufacturerOptions}
                        value={this.state.manufacturer}
                        optionRenderer={renderManufacturerOptionAndValue}
                        valueRenderer={renderManufacturerOptionAndValue}
                        disabled={!this.state.enabled}
                        placeholder="Выберите Фабрику"
                        clearable={false}
                        noResultsText="Производители не найдены"
                        onChange={this.handleManufacturerChange}
                    />
                </div>
                <div className="CollectionSelect-select CollectionSelect-select--collection">
                    <Select
                        options={this.collectionOptions}
                        value={this.state.collection}
                        optionRenderer={renderCollectionOptionAndValue}
                        valueRenderer={renderCollectionOptionAndValue}
                        disabled={!this.state.enabled || !this.state.manufacturer}
                        placeholder="Выберите коллекцию"
                        clearable={false}
                        noResultsText="Коллекции не найдены"
                        onChange={this.handleCollectionChange} />
                </div>
            </div>
        );
    }
}

export default CollectionSelect;
