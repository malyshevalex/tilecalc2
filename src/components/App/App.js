import React from 'react';
import classNames from 'classnames';

import AppActions from '../../actions/AppActions';
import ViewTypes from '../../constants/ViewTypes';
import Header from '../Header';

import Welcome from '../Welcome';
import RoomEditor from '../RoomEditor';
import CollectionSelector from '../CollectionSelector';

import Tooltip from '../Tooltip';
import DragElement from '../DragElement';

import fixtures from '../../settings.json';

import AppStore from '../../stores/AppStore';


import './App.styl';


const VIEWS = [
    {
        view: ViewTypes.WELCOME,
        element: Welcome
    },
    {
        step: 0,
        view: ViewTypes.ROOM_EDITOR,
        element: RoomEditor,
        title: 'Планировка помещения'
    },
    {
        step: 1,
        view: ViewTypes.COLLECTION_SELECTOR,
        element: CollectionSelector,
        title: 'Выбор коллекции'
    },
    {
        step: 2,
        title: 'Раскладки плитки'
    },
    {
        step: 3,
        title: 'Сохранение результата'
    }
];

const STEPS = VIEWS.filter(v => v.step !== undefined);

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            view: AppStore.getCurrentView()
        };
    }

    componentDidMount() {
        AppStore.on('change', () => { this.setState({ view: AppStore.getCurrentView() }); });
    }

    componentWillUnmount() {
        AppStore.off('change');
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.view !== nextState.view;
    }

    render() {
        let phone = fixtures.phone;
        let view = VIEWS.find(v => v.view === this.state.view);
        let step = view.step;
        let viewElement = React.createElement(view.element);
        let steps = '', hasSteps = (step !== undefined);
        if (hasSteps) {
            steps = (
                <ol className="Steps">
                    {
                        STEPS.map(v => {
                            let clickable = (v.step < step);
                            let clickHandler = clickable ? function() { AppActions.changeView(v.view); } : undefined;
                            return <li
                                className={classNames('Steps-step', {'Steps-step--active': (v.step === step), 'Steps-step--clickable': clickable})}
                                key={v.step}
                                onClick={clickHandler}>{v.title}</li>;
                        })
                    }
                </ol>
            );
        }

        return (
            <div className="App">
                <Header phone={phone} />
                {steps}
                <div className={classNames('View', {'View--withSteps': hasSteps})}>{viewElement}</div>
                <DragElement />
                <Tooltip />
            </div>
        );
    }
}


export default App;
