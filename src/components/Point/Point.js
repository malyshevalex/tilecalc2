import React from 'react';

import WallModel from '../../models/Wall';
import AbstractProjectElement from '../AbstractProjectElement';

import svgDefs from './Point.svg.defs';
import './Point.styl';

const PropTypes = React.PropTypes;

class SVGPoint extends AbstractProjectElement {

    static propTypes = {
        model: PropTypes.instanceOf(WallModel).isRequired,
        ratio: PropTypes.number,
        radius: PropTypes.number
    };

    static defaultProps = {
        ratio: 1,
        radius: 12
    };

    elementType = 'wall';

    componentWillMount() {
        svgDefs.use();
        super.componentWillMount();
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        svgDefs.unuse();
    }

    assignModel(wall) {
        this.setState({
            x: wall.x1,
            y: wall.y1
        });
    }

    render() {
        let {radius, ratio, ...props} = this.props;
        let transform = 'matrix(' + [ 1, 0, 0, 1, this.state.x * ratio, this.state.y * ratio ].join(',') + ')';
        return (
            <g transform={transform} className="Point" {...props}>
                <circle cx="0" cy="0" r={radius} className="Point-base" />
                <circle cx="0" cy="0" r={radius - 1} className="Point-ring" />
            </g>
        );
    }
}

export default SVGPoint;
