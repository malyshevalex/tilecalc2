import React from 'react';
import ReactDOM from 'react-dom';

import Viewport from '../../core/Viewport';
import Bounds from '../../models/Bounds';
import Grid from '../Grid';

import './SVGViewport.styl';

const PropTypes = React.PropTypes;

class SVGViewport extends React.Component {

    static propTypes = {
        bounds: PropTypes.instanceOf(Bounds).isRequired,
        scale: PropTypes.number,
        grid: PropTypes.bool
    };

    static defaultProps = {
        scale: .8,
        grid: false
    };

    constructor() {
        super();
        this.state = {
            width: 1,
            height: 1
        };
        this.ratio = 1;
        this.offset = { x: 0, y: 0 };
    }

    componentDidMount() {
        Viewport.onResize(this.handleResize, this);
        this.handleResize();
    }

    componentWillUnmount() {
        Viewport.off(this.handleResize, this);
    }

    componentWillUpdate(nextProps, nextState) {
        let scaledBounds = nextProps.bounds.scale(1 / nextProps.scale);
        let ratioX = nextState.width / (scaledBounds.x2 - scaledBounds.x1);
        let ratioY = nextState.height / (scaledBounds.y2 - scaledBounds.y1);
        this.ratio = Math.min(ratioX, ratioY);
        scaledBounds = nextProps.bounds.scale(this.ratio);
        this.offset = {
            x: (nextState.width - (scaledBounds.x2 - scaledBounds.x1)) / 2 - scaledBounds.x1,
            y: (nextState.height - (scaledBounds.y2 - scaledBounds.y1)) / 2 - scaledBounds.y1
        };
    }

    handleResize() {
        let wrap = ReactDOM.findDOMNode(this);
        this.setState({
            width: wrap.clientWidth,
            height: wrap.clientHeight
        });
    }

    createElements(elements) {
        if (!Array.isArray(elements)) {
            elements = [elements];
        }
        return elements.map(el => React.createElement(
            el.type,
            el.props,
            el.children ? this.createElements(el.children) : null
        ));
    }

    render() {
        let viewBox = [0, 0, this.state.width, this.state.height].join(' ');
        let ratio = this.ratio;
        let grid = this.props.grid ? <Grid offset={this.offset} ratio={ratio} /> : null;
        return (
            <div className="SVGViewport">
                <svg style={{ width: this.state.width, height: this.state.height }} viewBox={viewBox}>
                    {grid}
                    <g transform={'translate(' + this.offset.x + ' ' + this.offset.y + ')'}>
                        {React.Children.map(this.props.children, child => React.cloneElement(child, { ratio }))}
                    </g>
                </svg>
            </div>
        );
    }
}

export default SVGViewport;
