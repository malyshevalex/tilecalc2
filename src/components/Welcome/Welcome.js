import React from 'react';

import fixtures from '../../settings.json';
import RoomShape from '../RoomShape';
import AppActions from '../../actions/ProjectActions.js';

import './Welcome.styl';

class Welcome extends React.Component {

    static propTypes = {
        RoomShapes: React.PropTypes.array.isRequired
    };

    static defaultProps = {
        RoomShapes: fixtures.RoomShapes
    };

    newProjectClick = (e, roomShape) => {
        AppActions.createProject(roomShape.props.points);
    };

    render() {
        let RoomShapes = this.props.RoomShapes.map((tpl, i) => (
            <div className="Welcome-template" key={i}>
                <RoomShape points={tpl} width={90} height={90} onClick={this.newProjectClick} />
            </div>
        ));
        return (
            <div className="Welcome">
                <h1>Новый проект</h1>
                <h3>Выберите форму пола</h3>
                <div className="Welcome-templates">
                    {RoomShapes}
                </div>
            </div>
        );
    }
}

export default Welcome;
