import React from 'react';
import ReactDOM from 'react-dom';
import PureComponent from 'react-pure-render/component';
import classNames from 'classnames';

import float2str from '../../utils/float2str';

import './Input.styl';

const PropTypes = React.PropTypes;

const TYPES = {
    string: 'text',
    float: 'number',
    int: 'number'
};

const PATTERNS = {
    float: '[0-9]+([\.,][0-9]+)?',
    int: '[0-9]+'
};


class Input extends PureComponent {

    static propTypes = {
        label: PropTypes.string.isRequired,
        autoFocus: PropTypes.bool,
        unit: PropTypes.string,
        type: PropTypes.string,
        min: PropTypes.number,
        max: PropTypes.number,
        fillWidth: PropTypes.bool,
        onChange: PropTypes.func,
        onEnter: PropTypes.func,
        onEsc: PropTypes.func
    };

    static defaultProps = {
        autoFocus: false,
        fillWidth: false,
        unit: '',
        type: 'string'
    };

    componentDidMount() {
        if (this.props.autoFocus) {
            let node = ReactDOM.findDOMNode(this.refs['input']);
            node.focus();
            node.value = node.value; // dirty hack that moves cursor to the end of value
        }
    }

    componentWillMount() {
        this.setState({
            value: (this.props.value !== undefined) ? this.prepareValue(this.props.value) : ''
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: this.prepareValue(nextProps.value)
        });
    }

    prepareValue(value) {
        switch (this.props.type) {
            case 'float':
                return float2str(value);
            case 'int':
                return value.toString();
            default:
                return value;
        }
    }

    handleChange = (e) => {
        let value = e.target.value;
        let isValid = (this.parseValue(value) !== undefined);
        this.setState({
            error: !isValid,
            value
        });
    };

    handleBlur = (e) => {
        let value = this.parseValue(e.target.value);
        value = this.prepareValue(value !== undefined ? value : this.props.value);
        this.setState({ error: false, value });
    };

    handleKeyDown = (e) => {
        if (e.which === 13 && this.props.onEnter) {
            this.props.onEnter();
        } else if (e.which === 27 && this.props.onEsc) {
            this.props.onEsc();
        }
    };

    parseValue(value) {
        let type = this.props.type;
        let parsed;
        if (type === 'int' || type === 'float') {
            if (type === 'int') {
                parsed = parseInt(value);
            } else {
                parsed = parseFloat(value.replace(',', '.'));
            }
            if (Number.isNaN(parsed) || parsed > this.props.max || parsed < this.props.min) {
                parsed = undefined;
            }
        } else {
            parsed = value.trim();
        }
        if (parsed !== undefined && parsed !== this.props.value && this.props.onChange) {
            this.props.onChange(parsed);
        }
        return parsed;
    }

    render() {
        let {label, unit} = this.props;
        let style = {
            width: this.props.width
        };
        let pattern = PATTERNS[this.props.type];
        return (
            <span className={classNames('Input', { 'Input--error': this.state.error, 'Input--block': this.props.fillWidth })}>
                <span className="Input-label">{label}</span>
                <input className="Input-input" value={this.state.value}
                       onChange={this.handleChange} onBlur={this.handleBlur}
                       onKeyDown={this.handleKeyDown}
                       size={this.props.size}
                       step={this.props.step}
                       pattern={pattern}
                       type={TYPES.string}
                       style={style}
                       ref="input" />
                <span className="Input-unit">{unit}</span>
            </span>
        );
    }
}

export default Input;
