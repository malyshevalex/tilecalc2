import React from 'react';
import ReactDOM from 'react-dom';

import WallsEditorViewport from '../WallsEditorViewport';
import DropPointPopup from '../DropPointPopup';
import CreatePointPopup from '../CreatePointPopup';
import WallLengthPopup from '../WallLengthPopup';
import RoomArea from '../RoomArea';
import Button from '../Button';
import Input from '../Input';
import TooltipPoint from '../TooltipPoint';
import WallObjectIcon from '../WallObjectIcon';

import ProjectStore from '../../stores/ProjectStore';
import ProjectActions from '../../actions/ProjectActions';

import AppStore from '../../stores/AppStore';
import AppActions from '../../actions/AppActions';
import ViewTypes from '../../constants/ViewTypes';

import WallModel from '../../models/Wall';
import WallObject from '../../models/WallObject';

import normalizeAngle from '../../utils/normalizeAngle';
import { intersection } from '../../utils/ray';


import './WallsEditor.styl';

const
    MIN_SCALE = .25,
    MAX_SCALE = .9,
    SCALE_STEP = 0.05,
    ANGLE_SNAP = 2 * Math.PI / 180,
    GRID_SNAP = .1,
    GRID_SNAP_D = .02,
    RIGHT_ANGLE = Math.PI / 2;

class WallsEditor extends React.Component {

    constructor(props) {
        super(props);
        this.wallObjectIcons = WallObject.Types.map(type => {
            let startDragHandler = this.handleWallObjectStartDrag.bind(this, type);
            return <WallObjectIcon key={type} type={type} ref={'wallObjectIcon' + type} label={WallObject.getLabel(type)} onMouseDown={startDragHandler} onTouchStart={startDragHandler} />;
        });
        this.state = {
            scale: .75
        };
    }

    handlePointDragStart(index, e) {
        let ratio = this.refs.viewport.ratio;
        let oPoint = ProjectStore.getPoint(index),      // original point
            oX = oPoint.x,                              // original point place
            oY = oPoint.y,
            cPoints = ProjectStore.cloneScene().points, // clone points for previously moving
            cPoint = cPoints[index],                    // point to move
            oLeftAngle = oPoint.leftWall.angle,         // original left wall angle
            oRightAngle = oPoint.rightWall.angle,       // original right wall angle
            eX = -e.pageX / ratio + oX,                 // deltaX for drag
            eY = -e.pageY / ratio + oY,                 // deltaY for drag
            prev = cPoint.prev,                         // previous point
            next = cPoint.next;                         // next point

        this.pointDrag = {
            ratio, oX, oY, cPoint, oLeftAngle, oRightAngle, eX, eY, prev, next
        };
        this.setState({
            guides: [
                { x: oPoint.x, y: oPoint.y, angle: oPoint.leftWall.angle, ref: 'left-guide' },
                { x: oPoint.x, y: oPoint.y, angle: oPoint.rightWall.angle, ref: 'right-guide' },
                { x: oPoint.prev.x, y: oPoint.prev.y, angle: normalizeAngle(oPoint.prev.leftWall.angle + RIGHT_ANGLE), ref: 'prev-90-guide'},
                { x: oPoint.next.x, y: oPoint.next.y, angle: normalizeAngle(oPoint.next.rightWall.angle - RIGHT_ANGLE), ref: 'next-90-guide'}
            ]
        });
    }

    handlePointDrag(index, event) {
        let { ratio, oX, oY, cPoint, oLeftAngle, oRightAngle, eX, eY, prev, next } = this.pointDrag;
        let x = eX + event.pageX / ratio, y = eY + event.pageY / ratio;
        let leftGuide = false, rightGuide = false, prev90Guide = false, next90Guide = false;
        cPoint.moveTo(x, y);
        let sPoint = null;
        function snap() {
            if (sPoint && (sPoint.x !== x || sPoint.y !== y)) {
                x = sPoint.x;
                y = sPoint.y;
                cPoint.moveTo(x, y);
            }
        }

        if (Math.abs(cPoint.angle) < ANGLE_SNAP * 2) {
            if (prev.x === next.x) {
                sPoint = { x: prev.x, y };
            } else {
                let angle = Math.atan2(next.y - prev.y, next.x - prev.x);
                sPoint = intersection(x, y, normalizeAngle(angle + Math.PI / 2), prev.x, prev.y, angle);
            }
        } else {
            let dLeftAngle = Math.abs(normalizeAngle(cPoint.leftWall.angle - oLeftAngle));
            let dRightAngle = Math.abs(normalizeAngle(cPoint.rightWall.angle - oRightAngle));

            if (dLeftAngle < ANGLE_SNAP && dRightAngle < ANGLE_SNAP) {
                leftGuide = true;
                rightGuide = true;
                sPoint = { x: oX, y: oY };
            } else {
                if (dLeftAngle < ANGLE_SNAP) {
                    sPoint = intersection(prev.x, prev.y, oLeftAngle, x, y, normalizeAngle(oLeftAngle + RIGHT_ANGLE));
                    leftGuide = true;
                } else if (dRightAngle < ANGLE_SNAP) {
                    sPoint = intersection(x, y, normalizeAngle(oRightAngle + RIGHT_ANGLE), next.x, next.y, oRightAngle);
                    rightGuide = true;
                }
                snap();

                if (!leftGuide && Math.abs(Math.abs(prev.angle) - RIGHT_ANGLE) < ANGLE_SNAP) {
                    sPoint = intersection(prev.x, prev.y, normalizeAngle(prev.leftWall.angle + RIGHT_ANGLE), next.x, next.y, cPoint.rightWall.angle);
                    prev90Guide = true;
                }

                snap();

                if (!rightGuide && Math.abs(Math.abs(next.angle) - RIGHT_ANGLE) < ANGLE_SNAP) {
                    sPoint = intersection(next.x, next.y, normalizeAngle(next.rightWall.angle - RIGHT_ANGLE), prev.x, prev.y, cPoint.leftWall.angle);
                    next90Guide = true;
                }
            }
        }
        snap();
        if (!sPoint) {
            let rx = Math.round(x / GRID_SNAP) * GRID_SNAP,
                ry = Math.round(y / GRID_SNAP) * GRID_SNAP;
            if (Math.abs(rx - x) < GRID_SNAP_D) {
                x = rx;
            }
            if (Math.abs(ry - y) < GRID_SNAP_D) {
                y = ry;
            }

        }
        ProjectActions.movePoint(index, x, y);
        this.refs['left-guide'].setState({ visible: leftGuide });
        this.refs['right-guide'].setState({ visible: rightGuide });
        this.refs['prev-90-guide'].setState({ visible: prev90Guide });
        this.refs['next-90-guide'].setState({ visible: next90Guide });
    }

    handlePointDragStop() {
        delete this.pointDrag;
        this.setState({ guides: [] });
    }

    handlePointClick = (wall, element) => {
        if (WallModel.count(wall) > 3) {
            this.showPopup({
                type: DropPointPopup,
                anchor: element,
                alignment: 'L',
                onDrop: () => {
                    ProjectActions.dropPoint(wall);
                    this.closePopup();
                }
            });
        }
    };

    handleWallLengthClick = (wall, element) => {
        this.showPopup({
            type: WallLengthPopup,
            anchor: element,
            model: wall,
            onChange: (value) => ProjectActions.resizeWall(wall, value)
        });
    };

    handleWallClick = (wall, wallElement, e) => {
        let x = e.pageX, y = e.pageY;
        wallElement.setState({
            active: true
        });
        this.showPopup({
            type: CreatePointPopup,
            fromMouse: true,
            x, y,
            alignment: 'B',
            onCreate: () => {
                let rect = ReactDOM.findDOMNode(wallElement).getBoundingClientRect();
                let kx = (x - rect.left) / rect.width, ky = (y - rect.top) / rect.height;
                if (wall.x1 > wall.x2) {
                    kx = 1 - kx;
                }
                if (wall.y1 > wall.y2) {
                    ky = 1 - ky;
                }
                let px = wall.x1 + (wall.x2 - wall.x1) * kx,
                    py = wall.y1 + (wall.y2 - wall.y1) * ky;
                ProjectActions.insertPoint(wall, px, py);
                this.closePopup();
            },
            onClose: () => {
                wallElement.setState({
                    active: false
                });
                this.hidePopup();
            }
        });
    };

    showPopup(popup) {
        this.setState({ popup });
    }

    handleWallObjectStartDrag(type, e) {
        this.dragWallObjectType = type;
        let node = this.refs['wallObjectIcon' + type].getImgNode();
        let wallsEditorViewport = this.refs['wallsEditorViewport'];
        AppActions.startDrag(node, e, wallsEditorViewport.getWallsAreas());
        AppStore.on('drag.enter', this.handleWallObjectDragEnter, this);
        AppStore.on('drag.leave', this.handleWallObjectDragLeave, this);
        AppStore.on('drag.stop', this.handleWallObjectStopDrag, this);
    }

    handleWallObjectStopDrag(area) {
        AppStore.off('drag.enter', this.handleWallObjectDragEnter, this);
        AppStore.off('drag.leave', this.handleWallObjectDragLeave, this);
        AppStore.off('drag.stop', this.handleWallObjectStopDrag, this);
        if (area) {
            this.handleWallObjectDragLeave(area);
            ProjectActions.addObjectToWall(parseInt(area), this.dragWallObjectType);
        }
        delete this.dragWallObjectType;
    }

    handleWallObjectDragEnter(area) {
        this.refs['wallsEditorViewport'].wallHighlighting(area, true);
    }

    handleWallObjectDragLeave(area) {
        this.refs['wallsEditorViewport'].wallHighlighting(area, false);
    }

/*
    handleViewportWheel = (e) => {
        e.preventDefault();
        let direction = (e.deltaY) ? -e.deltaY / Math.abs(e.deltaY) : 0;
        this.setState({
            scale: Math.min(Math.max(this.state.scale + SCALE_STEP * direction, MIN_SCALE), MAX_SCALE)
        });
    };
*/

    handleWallObjectClick = (object) => {
        ProjectActions.editWallObject(object);
    };

    handleRotateSceneClick = () => {
        ProjectActions.rotateScene();
    };

    handleMirrorSceneClick = () => {
        ProjectActions.mirrorScene();
    };

    handleHeightChange = (height) => {
        ProjectActions.setHeight(height);
    };

    closePopup = () => {
        let popup = this.refs.popup;
        if (popup) {
            popup.close();
        }
    };

    hidePopup = () => {
        if (this.state.popup) {
            this.setState({ popup: undefined });
        }
    };

    handleCollectionSelectButtonClick = () => {
        AppActions.changeView(ViewTypes.COLLECTION_SELECTOR);
    };

    render() {
        let height = ProjectStore.getHeight();
        let projectName = ProjectStore.getName();
        let popup;
        if (this.state.popup) {
            let {type, ...props} = this.state.popup;
            props.ref = 'popup';
            if (!props.onClose) {
                props.onClose = this.hidePopup;
            }
            popup = React.createElement(type, props);
        }
        let heightConstraints = ProjectStore.getHeightConstraints();
        return (
            <div className="Editor WallsEditor" onMouseDown={this.closePopup}>
                <div className="Editor-palette Palette">
                    <div className="Palette-header">Параметры проекта</div>
                    <div className="Palette-section">
                        <Input label="Название проекта" type="string" fillWidth={true} value={projectName} />
                    </div>
                    <div className="Palette-section">
                        <Input label="Высота потолка" type="float" min={heightConstraints[0]} max={heightConstraints[1]} unit="м" width={150} value={height} onChange={this.handleHeightChange}/>
                    </div>
                    <div className="Palette-section">
                        <Button label="Повернуть пол" icon="rotate" onClick={this.handleRotateSceneClick} tooltip="Щелкнув несколько раз можно повернуть пол на 90, 180, 270 или 360 градусов"/>
                    </div>
                    <div className="Palette-section">
                        <Button label="Отразить зеркально" icon="flip" onClick={this.handleMirrorSceneClick} tooltip="При клике комната перевернется зеркально в горизонтальной плоскости" />
                    </div>
                    <div className="Palette-section">
                        <RoomArea label="Площадь: " />
                    </div>
                    <div className="Palette-header">Объекты <TooltipPoint tooltip="Объекты на планировке сокращают используемое количество плитки на свою площадь. Переместите объект на одну из стен"/></div>
                    <div className="Palette-section">
                        {this.wallObjectIcons}
                    </div>
                </div>
                <div className="Editor-editor">
                    <WallsEditorViewport
                        ref="wallsEditorViewport"
                        onPointClick={this.handlePointClick}
                        onWallLengthClick={this.handleWallLengthClick}
                        onWallClick={this.handleWallClick} />
                </div>
                <div className="Editor-actionsBar ActionsBar">
                    <Button label="Перейти к выбору коллекции" inline={true} type="primary" onClick={this.handleCollectionSelectButtonClick} />
                </div>
                {popup}
            </div>
        );
    }
}

export default WallsEditor;
