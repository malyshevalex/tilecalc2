import React from 'react';


class SVGGroup extends React.Component {

    static propTypes = {
        ratio: React.PropTypes.number
    };

    static defaultProps = {
        ratio: 1
    };

    render() {
        let {ratio, ...props} = this.props;
        return (
            <g {...props}>
                {React.Children.map(this.props.children, child => React.cloneElement(child, { ratio }))}
            </g>
        );
    }
}

export default SVGGroup;
