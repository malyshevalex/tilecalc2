import React from 'react';

import ProjectStore from '../../stores/ProjectStore';
import Wall from '../../models/Wall';
import float2str from '../../utils/float2str';

import AbstractProjectElement from '../AbstractProjectElement';

import './WallBack.styl';


class SVGWallBack extends AbstractProjectElement {
    static propTypes = {
        model: React.PropTypes.instanceOf(Wall).isRequired,
        ratio: React.PropTypes.number
    };

    static defaultProps = {
        ratio: 1
    };

    elementType = 'wall';

    assignModel(wall) {
        this.setState({
            length: wall.length,
            height: ProjectStore.getHeight()
        });
    }

    render() {
        let ratio = this.props.ratio;
        let width = this.state.length * ratio;
        let height = this.state.height * ratio;
        let lengthText = float2str(this.state.length, 'м'),
            heightText = float2str(this.state.height, 'м');
        return (
            <g className="WallBack">
                <rect className="WallBack-rect" width={width} height={height} />
                <text className="WallBack-size" x={width / 2} y={height + 16} textAnchor="middle">{lengthText}</text>
                <text className="WallBack-size" x={-6} y={height / 2} textAnchor="end">{heightText}</text>
            </g>

        );
    }
}

export default SVGWallBack;
