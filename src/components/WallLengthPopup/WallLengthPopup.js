import React from 'react';

import popup from '../../decorators/popup';
import AbstractProjectElement from '../AbstractProjectElement';
import Input from '../Input';
import WallModel from '../../models/Wall';

class WallLengthPopup extends AbstractProjectElement {

    static propTypes = {
        model: React.PropTypes.instanceOf(WallModel),
        onChange: React.PropTypes.func
    };

    elementType = 'wall';

    componentDidUpdate() {
        this.props.place();
    }

    assignModel(wallModel) {
        this.setState({
            value: wallModel.length
        });
    }

    handleChange(val) {
        if (this.props.onChange) {
            this.props.onChange(val);
        }
    }

    emitClose = () => { this.props.close(); };

    render() {
        return (
            <Input label="Длина стены" unit="м" width="100" autoFocus={true}
                   value={this.state.value} type="float" step="0.1"
                   min={WallModel.getMinLength()} max={WallModel.getMaxLength()}
                   onChange={(val) => this.handleChange(val)}
                   onEnter={this.emitClose}
                   onEsc={this.emitClose}
            />
        );
    }
}

export default popup(WallLengthPopup);
