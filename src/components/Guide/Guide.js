import React from 'react';
import PureComponent from 'react-pure-render/component';
import classNames from 'classnames';

import './Guide.styl';

const PropTypes = React.PropTypes;
const GUIDE_LENGTH = 2000;

class SVGGuide extends PureComponent {

    static propTypes = {
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired,
        angle: PropTypes.number.isRequired,
        ratio: PropTypes.number
    };

    static defaultProps = {
        ratio: 1
    };

    constructor() {
        super();
        this.state = {
            visible: false
        };
    }

    render() {
        let { ratio, x, y, angle } = this.props;
        let dx = Math.cos(angle) * GUIDE_LENGTH, dy = Math.sin(angle) * GUIDE_LENGTH;
        x *= ratio;
        y *= ratio;
        return (
            <line x1={x - dx} y1={y - dy} x2={x + dx} y2={y + dy} className={classNames('Guide', { 'Guide--visible': this.state.visible })} />
        );
    }
}

export default SVGGuide;
