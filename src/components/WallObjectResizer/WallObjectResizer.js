import React from 'react';

import ProjectStore from '../../stores/ProjectStore';
import WallObject from '../../models/WallObject';

import Point from '../Point';

const PropTypes = React.PropTypes;

class SVGWallObjectResizer extends Point {
    static propTypes = {
        model: PropTypes.instanceOf(WallObject).isRequired,
        ratio: PropTypes.number,
        radius: PropTypes.number
    };

    elementType = 'object';

    assignModel(object) {
        this.setState({
            x: object.right,
            y: ProjectStore.getHeight() - object.top
        });
    }
}

export default SVGWallObjectResizer;
