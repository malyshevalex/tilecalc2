import React from 'react';

import CollectionsStore from '../../stores/CollectionsStore';
import SurfaceDirectionTypes from '../../constants/SurfaceDirectionTypes';

import TooltipPoint from '../TooltipPoint';

import float2str from '../../utils/float2str';

import './CollectionSurfaceOptions.styl';

const PropTypes = React.PropTypes;

const SURFACE_TYPE_LABELS = {
    [SurfaceDirectionTypes.HORIZONTAL]: 'Прямоугольная горизонтально',
    [SurfaceDirectionTypes.VERTICAL]: 'Прямоугольная вертикально',
    [SurfaceDirectionTypes.SQUARE]: 'Горизонтально',
    [SurfaceDirectionTypes.DIAGONAL]: 'Диагонально'
};


class CollectionSurfaceOptions extends React.Component {

    static propTypes = {
        enabled: PropTypes.bool,
        title: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        manufacturer: PropTypes.string,
        collection: PropTypes.string,
        jointWidth: PropTypes.number,
        direction: PropTypes.string,
        onChange: PropTypes.func
    };

    static defaultProps = {
        enabled: false
    };

    componentWillMount() {
        this.prepare(this.props);
    }

    shouldComponentUpdate(nextProps) {
        return (this.props.collection !== nextProps.collection || this.props.direction !== nextProps.direction);
    }

    componentWillUpdate(nextProps) {
        this.prepare(nextProps);
    }

    prepare(props) {
        this.collection = CollectionsStore.getCollection(props.type, props.manufacturer, props.collection);
        this.manufacturer = CollectionsStore.getManufacturer(props.manufacturer);
        this.directionTypes = CollectionsStore.getAvailableDirectionTypes(this.collection);
    }

    handleJointWidthChange = (e) => {
        this.changeOption('jointWidth', parseFloat(e.target.value));
    };

    handleDirectionChange = (e) => {
        this.changeOption('direction', e.target.value);
    };

    changeOption(name, value) {
        if (this.props.onChange) {
            this.props.onChange.call(null, name, value);
        }
    }

    getKey(v) {
        return this.props.manufacturer + '_' + this.props.collection + '_' + v;
    }

    render() {
        let titleHint = '';
        if (this.props.enabled && this.collection && this.manufacturer) {
            titleHint = <span className="CollectionSurfaceOptions-titleHint">{this.manufacturer.name} {this.collection.name}</span>;
        }

        return (
            <div className="CollectionSurfaceOptions">
                <h2>{this.props.title}{titleHint}</h2>
                <div className="CollectionSurfaceOptions-section">
                    <h3>Ширина шва между плитками <TooltipPoint tooltip="Промежуток между двумя плитками" /></h3>
                    {CollectionsStore.getAvailableJointWidths().map((w) => {
                        return (
                            <label key={this.getKey(w)} className="CollectionSurfaceOptions-check CollectionSurfaceOptions-check--joint">
                                <input type="radio" name={`jointWidth_${this.props.type}`} value={w} defaultChecked={this.props.jointWidth === w} onChange={this.handleJointWidthChange} />
                                {float2str(w, 'мм')}
                            </label>
                        );
                    })}
                </div>
                <div className="CollectionSurfaceOptions-section">
                    <h3>Вариант раскладки <TooltipPoint tooltip="Положение плитки на стене" /></h3>
                    {this.directionTypes.map((d) => {
                        return (
                            <label key={this.getKey(d)} className="CollectionSurfaceOptions-check CollectionSurfaceOptions-check--direction">
                                <input type="radio" name={`direction_${this.props.type}`} value={d} defaultChecked={this.props.direction === d} onChange={this.handleDirectionChange} />
                                <span className={`CollectionSurfaceOptions-directionIcon CollectionSurfaceOptions-directionIcon--${d}`} />
                                {SURFACE_TYPE_LABELS[d]}
                            </label>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default CollectionSurfaceOptions;
