import React from 'react';

import ProjectStore from '../../stores/ProjectStore';
import ProjectActions from '../../actions/ProjectActions';
import WallObject from '../../models/WallObject';

import AbstractProjectElement from '../AbstractProjectElement';

import Input from '../Input';

import './WallObjectMetrics.styl';


class SVGWallObject extends AbstractProjectElement {
    static propTypes = {
        model: React.PropTypes.instanceOf(WallObject).isRequired
    };

    elementType = 'object';

    assignModel(object) {
        this.setState({
            left: object.left,
            bottom: object.bottom,
            width: object.width,
            height: object.height
        });
    }

    handleLeftChange = (value) => {
        ProjectActions.moveWallObject(this.props.model, value, this.state.bottom);
    };

    handleBottomChange = (value) => {
        ProjectActions.moveWallObject(this.props.model, this.state.left, value);
    };

    handleWidthChange = (value) => {
        ProjectActions.resizeWallObject(this.props.model, value, this.state.height);
    };

    handleHeightChange = (value) => {
        ProjectActions.resizeWallObject(this.props.model, this.state.width, value);
    };

    render() {
        let object = this.props.model;
        let c = object.constraints;
        let name = WallObject.getGenitive(object.type);
        let wallLength = ProjectStore.getWall(object.wallIndex).length;
        let wallHeight = ProjectStore.getHeight();
        let bottomInput = (object.type !== WallObject.DOOR)
            ? <div className="Palette-section"><Input label="От пола" unit="м" min={0} max={wallHeight - this.height} type="float" value={this.state.bottom} width={150} onChange={this.handleBottomChange} /></div>
            : '';
        return (
            <div>
                <div className="Palette-header">Параметры {name}</div>
                <div className="Palette-section">
                    <Input label="От левого края" unit="м" min={0} max={wallLength - this.state.width} type="float" value={this.state.left} width={150} onChange={this.handleLeftChange} />
                </div>
                {bottomInput}
                <div className="Palette-section">
                    <Input label="Ширина" unit="м" min={c.width[0]} max={Math.min(c.width[1], wallLength - this.state.left)} type="float" value={this.state.width} width={150} onChange={this.handleWidthChange} />
                </div>
                <div className="Palette-section">
                    <Input label="Высота" unit="м" min={c.height[0]} max={Math.min(c.height[1], wallHeight - this.state.bottom)} type="float" value={this.state.height} width={150} onChange={this.handleHeightChange} />
                </div>
            </div>
        );
    }
}

export default SVGWallObject;
