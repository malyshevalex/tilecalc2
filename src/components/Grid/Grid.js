import React from 'react';

import './Grid.styl';

const PropTypes = React.PropTypes;

class Grid extends React.Component {

    static propTypes = {
        ratio: PropTypes.number,
        offset: PropTypes.shape({ x: PropTypes.number.isRequired, y: PropTypes.number.isRequired }),
        small: PropTypes.number
    };

    static defaultProps = {
        ratio: 1,
        offset: { x: 0, y: 0 },
        small: 0.1
    };

    render() {
        let { ratio, offset, small } = this.props;
        small *= ratio;
        return (
            <g transform={'translate(' + [offset.x, offset.y].join(' ') + ')'}>
                <defs>
                    <pattern id="smallGrid" width={small} height={small} patternUnits="userSpaceOnUse">
                        <path d={['M', small, 0, 'L', 0, 0, 0, small].join(' ')} className="Grid-small" />
                    </pattern>
                    <pattern id="grid" width={ratio} height={ratio} patternUnits="userSpaceOnUse">
                        <rect width={ratio} height={ratio} fill="url(#smallGrid)"/>
                        <path d={['M', ratio, 0, 'L', 0, 0, 0, ratio].join(' ')} className="Grid-large" />
                    </pattern>
                </defs>
                <rect x="-100%" y="-100%" width="200%" height="200%" fill="url(#grid)" />
            </g>
        );
    }
}

export default Grid;
