import React from 'react';  //  eslint-disable-line no-unused-vars
import classNames from 'classnames';

import float2str from '../../utils/float2str';

import SVGWall from '../Wall';

import './WallLength.styl';


class SVGWallLength extends SVGWall {

    assignModel(wall) {
        this.setState({
            x: wall.x1,
            y: wall.y1,
            length: wall.length,
            angle: wall.angle,
            fromAngle: wall.leftAngle
        });
    }

    render() {
        let {ratio, thickness, ...props} = this.props;
        let angle = this.state.angle;
        let transform = 'matrix(' + this.getTransformMatrix(ratio).join(',') + ')';
        let width = this.state.length * ratio;
        let corrAngle = 0;
        let offset = thickness / 2;
        if ((angle >= Math.PI / 2) || (angle < -Math.PI / 2)) {
            corrAngle = 180;
        } else {
            if (this.state.fromAngle < -Math.PI / 4) {
                offset -= thickness * 2;
            } else {
                offset += thickness;
            }
        }
        let className = classNames('WallLength', {
            'WallLength--sm': (width < 100 && width > 40),
            'WallLength--xs': width <= 40
        });
        return (
            <g transform={transform} className="Wall" {...props}>
                <g transform={'translate(' + (width / 2) + ' 0)'}>
                    <text x="0" y={-offset} transform={'rotate(' + corrAngle + ')'} className={className}>{float2str(this.state.length, 'м')}</text>
                </g>
            </g>
        );
    }
}

export default SVGWallLength;
