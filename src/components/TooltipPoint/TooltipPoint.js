import React from 'react';
import ReactDOM from 'react-dom';
import PureComponent from 'react-pure-render/component';
import AppActions from '../../actions/AppActions';

import './TooltipPoint.styl';

class TooltipPoint extends PureComponent {
    static propTypes = {
        tooltip: React.PropTypes.string.isRequired
    };

    handleMouseEnter = () => {
        let rect = ReactDOM.findDOMNode(this).getBoundingClientRect();
        AppActions.showTooltip(rect.left + rect.width / 2, rect.top, this.props.tooltip);
    };

    handleMouseLeave = () => {
        AppActions.hideTooltip();
    };

    render() {
        return (
            <div className="TooltipPoint" onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} ref="point"></div>
        );
    }
}

export default TooltipPoint;

