import React from 'react';

import draggable from '../../decorators/draggable';

import ProjectActions from '../../actions/ProjectActions.js';
import ProjectStore from '../../stores/ProjectStore';
import Bounds from '../../models/Bounds';

import SVGViewport from '../SVGViewport';
import WallBack from '../WallBack';
import WallObject from '../WallObject';
import WallObjectResizer from '../WallObjectResizer';
import WallObjectMetrics from '../WallObjectMetrics';
import WallObjectSizes from '../WallObjectSizes';
import Button from '../Button';

import './WallObjectsEditor.styl';

const DraggableWalObject = draggable(WallObject);
const DraggableWallObjectResizer = draggable(WallObjectResizer);

class WallObjectsEditor extends React.Component {

    constructor() {
        super();
        this.state = {
            scale: .75
        };
    }

    componentWillMount() {
        this.prepareObjects(this.props.object);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.object !== nextProps.object) {
            this.prepareObjects(nextProps.object);
        }
    }

    prepareObjects(object) {
        this.objectNodes = ProjectStore.getWallObjects(object.wallIndex)
            .filter(o => o !== object)
            .map((o, i) => <DraggableWalObject model={o} key={i}
                                               onDragStart={this.objectDragStart.bind(this, o)}
                                               onDragStop={this.handleObjectDragStop}
                                               onDrag={this.handleObjectDrag}/>);
    }

    objectDragStart(object, e) {
        if (!e) {
            e = object;
            object = this.props.object;
        }
        if (this.props.object !== object) {
            ProjectActions.editWallObject(object);
        }
        let ratio = this.refs.viewport.ratio;
        let oLeft = object.left,
            oBottom = object.bottom,
            eLeft = -e.pageX / ratio + oLeft,
            eBottom = e.pageY / ratio + oBottom;

        this.dragInfo = {
            ratio, oLeft, oBottom, eLeft, eBottom, object
        };
    }

    objectDrag(e) {
        let { ratio, eLeft, eBottom, object } = this.dragInfo;
        let left = eLeft + e.pageX / ratio,
            bottom = eBottom - e.pageY / ratio;
        ProjectActions.moveWallObject(object, left, bottom);
    }

    dragStop(/* object, event */) {
        delete this.dragInfo;
    }

    resizerDragStart(e) {
        let ratio = this.refs.viewport.ratio;
        let o = this.props.object;
        let oWidth = -e.pageX / ratio + o.width,
            oHeight = e.pageY / ratio + o.height;
        this.dragInfo = { oWidth, oHeight, ratio };
    }

    resizerDrag(e) {
        let { oWidth, oHeight, ratio } = this.dragInfo;
        let width = oWidth + e.pageX / ratio,
            height = oHeight - e.pageY / ratio;
        ProjectActions.resizeWallObject(this.props.object, width, height);
    }

    handleObjectDrag = (e) => this.objectDrag(e);

    handleObjectDragStop = (e) => this.dragStop(e);

    handleResizerDragStart = (e) => this.resizerDragStart(e);

    handleResizerDrag = (e) => this.resizerDrag(e);

    handleResizerDragStop = (e) => this.dragStop(e);

    handleBackClick = () => {
        ProjectActions.saveWallObject(this.props.object);
    };

    handleDeleteClick = () => {
        ProjectActions.deleteWallObject(this.props.object);
    };

    render() {
        let wall = ProjectStore.getWall(this.props.object.wallIndex);
        let bounds = new Bounds(0, 0, wall.length, ProjectStore.getHeight());

        return (
            <div className="Editor WallObjectsEditor">
                <div className="Editor-palette Palette">
                    <WallObjectMetrics model={this.props.object} />
                </div>
                <div className="Editor-editor">
                    <SVGViewport bounds={bounds} grid={false} ref="viewport" scale={this.state.scale}>
                        <WallBack model={wall} />
                        {this.objectNodes}
                        <WallObjectSizes model={this.props.object} length={wall.length} height={ProjectStore.getHeight()} />
                        <DraggableWalObject model={this.props.object}
                                            active={true}
                                            onDragStart={this.objectDragStart.bind(this)}
                                            onDragStop={this.handleObjectDragStop}
                                            onDrag={this.handleObjectDrag} />
                        <DraggableWallObjectResizer model={this.props.object}
                                                    onDragStart={this.handleResizerDragStart}
                                                    onDragStop={this.handleResizerDragStop}
                                                    onDrag={this.handleResizerDrag} />
                    </SVGViewport>
                </div>
                <div className="Editor-actionsBar ActionsBar">
                    <Button label="Сохранить" onClick={this.handleBackClick} inline={true} type="primary" />
                    <Button label="Удалить" onClick={this.handleDeleteClick} inline={true} type="cancel" />
                </div>
            </div>
        );
    }
}

export default WallObjectsEditor;

