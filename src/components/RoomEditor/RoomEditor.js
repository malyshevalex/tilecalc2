import React from 'react';

import ProjectStore from '../../stores/ProjectStore';

import WallsEditor from '../WallsEditor';
import WallObjectsEditor from '../WallObjectsEditor';

class RoomEditor extends React.Component {

    constructor() {
        super();
        this.state = {
        };
    }

    componentDidMount() {
        ProjectStore.on('object.edit', this.handleObjectEdit, this);
        ProjectStore.on('object.save', this.handleObjectSave, this);
        ProjectStore.on('object.delete', this.handleObjectDelete, this);
    }

    componentWillUnmount() {
        ProjectStore.off('object.edit', this.handleObjectEdit, this);
        ProjectStore.off('object.save', this.handleObjectSave, this);
        ProjectStore.off('object.delete', this.handleObjectDelete, this);
    }

    handleObjectEdit(object) {
        this.setState({
            objectEdit: object
        });
    }

    handleObjectSave(/*object*/) {
        this.setState({
            objectEdit: undefined
        });
    }

    handleObjectDelete(/*object*/) {
        this.setState({
            objectEdit: undefined
        });
    }

    render() {
        if (this.state.objectEdit) {
            return <WallObjectsEditor object={this.state.objectEdit} />;
        } else {
            return <WallsEditor />;
        }
    }
}

export default RoomEditor;

