import React from 'react';
import './Header.styl';

//import catalogHTML from '../../assets/catalog.html';

class Header extends React.Component {

    static propTypes = {
        phone: React.PropTypes.string.isRequired
    };

    render() {
        return (
            <header className="Header">
                <div className="Header-logo"></div>
                {/*                <div className="Header-catalog" dangerouslySetInnerHTML={{__html: catalogHTML}}></div> */}
                <div className="Header-phone">{ this.props.phone }</div>
            </header>
        );
    }
}

export default Header;
