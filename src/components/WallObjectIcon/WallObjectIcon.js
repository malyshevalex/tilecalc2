import React from 'react';
import ReactDOM from 'react-dom';
import PureComponent from 'react-pure-render/component';

import WallObject from '../../models/WallObject';

import './WallObjectIcon.styl';
import areaImgSrc from './img/area.png';
import doorImgSrc from './img/door.png';
import windowImgSrc from './img/window.png';

const PropTypes = React.PropTypes;

class WallObjectIcon extends PureComponent {

    static propTypes = {
        label: PropTypes.string.isRequired,
        type: PropTypes.number.isRequired
    };

    getImgNode() {
        return ReactDOM.findDOMNode(this.refs.img);
    }

    render() {
        let { type, label, ...props } = this.props;
        let src;
        switch (type) {
            case WallObject.DOOR:
                src = doorImgSrc;
                break;
            case WallObject.WINDOW:
                src = windowImgSrc;
                break;
            default:
                src = areaImgSrc;
        }
        return (
            <div className="WallObjectIconWrapper">
                <div className="WallObjectIcon" {...props}>
                    <span className="WallObjectIcon-label">{label}</span>
                    <div className="WallObjectIcon-img" ref="img">
                        <img src={src} />
                    </div>
                </div>
            </div>
        );
    }
}

export default WallObjectIcon;

