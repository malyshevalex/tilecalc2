import React from 'react';

import ProjectStore from '../../stores/ProjectStore';

import SVGViewport from '../SVGViewport';
import Wall from '../Wall';
import WallLength from '../WallLength';
import Point from '../Point';
import Guide from '../Guide';
import draggable from '../../decorators/draggable';
import WallModel from '../../models/Wall';
import dummyFn from '../../utils/dummyFn';

const DraggablePoint = draggable(Point);

const WALL_RE = /^wall\d+$/;

class WallsEditorViewport extends React.Component {

    static propTypes = {
        scale: React.PropTypes.number,
        onPointClick: React.PropTypes.func,
        onWallLengthClick: React.PropTypes.func,
        onWallClick: React.PropTypes.func
    };

    static defaultProps = {
        scale: .75,
        onPointClick: dummyFn,
        onWallLengthClick: dummyFn,
        onWallClick: dummyFn
    };

    constructor(props) {
        super(props);
        this.state = {
            wallsHead: ProjectStore.getWallsHead(),
            guides: null
        };
    }

    componentDidMount() {
        ProjectStore.on('room.change', this.handleRoomChange, this);
    }

    componentWillUnmount() {
        ProjectStore.off('room.change', this.handleRoomChange, this);
    }

    handleRoomChange() {
        this.setState({
            wallsHead: ProjectStore.getWallsHead()
        });
    }

    getWallsAreas() {
        let areas = [];
        for (let key in this.refs) {
            if (this.refs.hasOwnProperty(key) && key.match(WALL_RE)) {
                areas.push(this.refs[key].getBoundingClientPoly());
            }
        }
        return areas;
    }

    wallHighlighting(i, active) {
        this.refs['wall' + i].setState({
            active: !!active
        });
    }

    render() {
        let guides = [];
        if (this.state.guides && this.state.guides.length) {
            guides = this.state.guides.map((guide, i) => {
                let {type, ...props} = guide;
                props.key = i;
                return React.createElement(type || Guide, props);
            });
        }
        let bounds = WallModel.getWallsBounds(this.state.wallsHead);

        let walls = [], lengths = [], points = [];
        WallModel.forEach(this.state.wallsHead, (wall, i) => {
            let wallRef = 'wall' + i;
            let lengthRef = 'wallLength' + i;
            let pointRef = 'point' + i;
            walls.push(<Wall model={wall} ref={wallRef} key={i}
                             onClick={(e) => this.props.onWallClick(wall, this.refs[wallRef], e)}
                             onObjectClick={this.handleWallObjectClick} />);
            lengths.push(<WallLength model={wall} ref={lengthRef} key={i}
                                     onClick={() => this.props.onWallLengthClick(wall, this.refs[lengthRef])} />);
            points.push(<DraggablePoint model={wall} key={i} ref={pointRef}
                                        onClick={() => this.props.onPointClick(wall, this.refs[pointRef])} />);
        });
        return (
            <SVGViewport bounds={bounds} grid={true} scale={this.props.scale} ref="viewport">
                {walls}
                {lengths}
                {points}
                {guides}
            </SVGViewport>
        );
    }
}

//{this.state.points.map((point, i) => <DraggablePoint model={point} key={i} ref={'point' + i}
//                                                     onDragStart={this.handlePointDragStart.bind(this, i)}
//                                                     onDrag={this.handlePointDrag.bind(this, i)}
//                                                     onDragStop={this.handlePointDragStop.bind(this, i)}
//                                                     onClick={this.handlePointClick.bind(this, i)} />)}


export default WallsEditorViewport;
