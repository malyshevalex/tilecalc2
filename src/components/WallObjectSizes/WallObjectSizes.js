import React from 'react';

import cm from '../../utils/cm';
import float2str from '../../utils/float2str';
import ProjectStore from '../../stores/ProjectStore';
import WallObject from '../../models/WallObject';

import AbstractProjectElement from '../AbstractProjectElement';

import './WallObjectSizes.styl';

const PropTypes = React.PropTypes;

class SVGWallObjectSizes extends AbstractProjectElement {
    static propTypes = {
        model: PropTypes.instanceOf(WallObject).isRequired,
        ratio: PropTypes.number,
        spacing: PropTypes.number
    };

    static defaultProps = {
        ratio: 1,
        spacing: 16
    };

    elementType = 'object';

    assignModel(object) {
        this.setState({
            left: object.left,
            top: object.top,
            right: object.right,
            bottom: object.bottom,
            width: object.width,
            height: object.height
        });
    }

    sizeText(key, x, y, value, vertical) {
        return <text className="WallObjectSizes-text" key={key} x={x} y={y} textAnchor={vertical ? 'start' : 'middle'}>{float2str(value)}</text>;
    }

    render() {
        let {model, spacing, ratio} = this.props;
        let oLength = ProjectStore.getWall(model.wallIndex).length,
            oHeight = ProjectStore.getHeight();
        let wallHeight = oHeight * ratio,
            wallLength = oLength * ratio;
        let s2 = spacing * 2;
        let sLeft = this.state.left * ratio,
            sTop = wallHeight - this.state.top * ratio,
            sRight = this.state.right * ratio,
            sBottom = wallHeight - this.state.bottom * ratio;

        let dPath = `M0 0 l0 ${-s2} M${wallLength} 0 l0 ${-s2} M${wallLength} 0 l${s2} 0 M${wallLength} ${wallHeight} l${s2} 0`;
        let sizes = [
            this.sizeText('width', sLeft + this.state.width * ratio / 2, -spacing, this.state.width),
            this.sizeText('height', wallLength + spacing, sTop + this.state.height * ratio / 2, this.state.height, true)
        ];
        if (sLeft > 0) {
            dPath += ` M${sLeft} ${sTop} L${sLeft} ${-s2}`;
            sizes.push(this.sizeText('left', sLeft / 2, -spacing, this.state.left));
        }
        if (sRight < wallLength) {
            dPath += ` M${sRight} ${sTop} L${sRight} ${-s2}`;
            sizes.push(this.sizeText('right', (wallLength + sRight) / 2, -spacing, cm.round(oLength - this.state.right)));
        }
        if (sTop > 0) {
            dPath += ` M${sRight} ${sTop} L${wallLength + s2} ${sTop}`;
            sizes.push(this.sizeText('top', wallLength + spacing, sTop / 2, cm.round(oHeight - this.state.top), true));
        }
        if (sBottom < wallHeight) {
            dPath += ` M${sRight} ${sBottom} L${wallLength + s2} ${sBottom}`;
            sizes.push(this.sizeText('bottom', wallLength + spacing, (wallHeight + sBottom) / 2, this.state.bottom, true));
        }

        return (
            <g className="WallObjectSizes">
                <path d={dPath} />
                {sizes}
            </g>
        );
    }
}

export default SVGWallObjectSizes;
