import React from 'react';
import PureComponent from 'react-pure-render/component';
import classNames from 'classnames';

import TooltipPoint from '../TooltipPoint';

import './Button.styl';

const PropTypes = React.PropTypes;

class Button extends PureComponent {

    static propTypes = {
        label: PropTypes.string.isRequired,
        icon: PropTypes.string,
        tooltip: PropTypes.string,
        type: PropTypes.string,
        inline: PropTypes.bool
    };

    static defaultProps = {
        icon: null,
        tooltip: '',
        inline: false
    };

    render() {
        let icon = (this.props.icon) ? <span className={classNames('Button-icon', 'Button-icon--' + this.props.icon)} /> : '';
        let tooltipPoint = this.props.tooltip ? <span className="Button-tooltip"><TooltipPoint tooltip={this.props.tooltip} /></span> : '';
        let className = classNames('Button', {
            'Button--inline': this.props.inline,
            'Button--withTooltip': tooltipPoint
        }, this.props.type ? 'Button--' + this.props.type : null);
        return (
            <span className={className}>
                {tooltipPoint}
                <span className="Button-btn" onClick={this.props.onClick}>
                    {icon}
                    {this.props.label}
                </span>
            </span>
        );
    }
}

export default Button;
