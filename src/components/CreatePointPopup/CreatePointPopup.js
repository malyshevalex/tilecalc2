
import React from 'react';
import popup from '../../decorators/popup';

import Button from '../Button';


class CreatePointPopup extends React.Component {

    static propTypes = {
        onCreate: React.PropTypes.func
    };

    handleClick = () => {
        if (this.props.onCreate) {
            this.props.onCreate();
        }
    };

    render() {
        return (
            <Button label="Добавить точку преломления" icon="newPoint" onClick={this.handleClick} />
        );
    }
}

export default popup(CreatePointPopup);
