import React from 'react';  //  eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import PureComponent from 'react-pure-render/component';

import AppStore from '../../stores/AppStore';
import './Tooltip.styl';

const HIDDEN = {
    visible: false,
    text: '',
    top: -1000,
    left: -1000
};

class Tooltip extends PureComponent {

    constructor() {
        super();
        this.state = {
            ...HIDDEN
        };
    }

    componentDidMount() {
        AppStore.on('tooltip.show', this.handleShow, this);
        AppStore.on('tooltip.hide', this.handleHide, this);
    }


    componentWillUnmount() {
        AppStore.off('tooltip.show', this.handleShow, this);
        AppStore.off('tooltip.hide', this.handleHide, this);
    }

    handleShow() {
        let tooltip = AppStore.getTooltip();
        let pHeight = ReactDOM.findDOMNode(this).offsetParent.clientHeight;
        this.setState({
            visible: true,
            left: tooltip.x,
            bottom: pHeight - tooltip.y,
            text: tooltip.text
        });
    }

    handleHide() {
        this.setState(HIDDEN);
    }

    render() {
        let style = {
            left: this.state.left,
            bottom: this.state.bottom
        };
        return (
            <div style={style} className={classNames('Tooltip', {'Tooltip--visible': this.state.visible})}>{this.state.text}</div>
        );
    }
}

export default Tooltip;

