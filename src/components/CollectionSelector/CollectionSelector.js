import React from 'react';  //  eslint-disable-line no-unused-vars
import PureComponent from 'react-pure-render/component';

import ProjectStore from '../../stores/ProjectStore';
import CollectionsStore from '../../stores/CollectionsStore';
import SurfaceTypes from '../../constants/SurfaceTypes';
import AppActions from '../../actions/AppActions';
import ViewTypes from '../../constants/ViewTypes';

import CollectionSurface from '../CollectionSurface';
import Button from '../Button';

import './CollectionSelector.styl';

function doesFloorNedPreview() {
    let needWalls = ProjectStore.needSurface(SurfaceTypes.WALLS);
    let wallsCollection = ProjectStore.getCollection(SurfaceTypes.WALLS);
    let floorCollection = ProjectStore.getCollection(SurfaceTypes.FLOOR);
    return Boolean(floorCollection[1] && (!needWalls || wallsCollection[0] !== floorCollection[0] || wallsCollection[1] !== floorCollection[1]));
}

class CollectionSelector extends PureComponent {

    componentWillMount() {
        this.setState({
            ready: CollectionsStore.isReady(),
            floorNeedPreview: doesFloorNedPreview()
        });
    }
    componentDidMount() {
        if (!CollectionsStore.isReady()) {
            CollectionsStore.on('ready',  this.handleCollectionsReady, this);
        }
        ProjectStore.on('surface.collectionChange', this.handleSurfaceChange, this);
    }

    componentWillUnmount() {
        CollectionsStore.off('ready',  this.handleCollectionsReady, this);
        ProjectStore.off('surface.collectionChange', this.handleSurfaceChange, this);
    }

    handleCollectionsReady() {
        this.setState({
            ready: true
        });
    }

    handleSurfaceChange() {
        this.setState({
            floorNeedPreview: doesFloorNedPreview()
        });
    }

    handleConfirmButton() {
        AppActions.changeView(ViewTypes.LAYOUT_EDITOR);
    }

    render() {
        if (!this.state.ready) {
            return <div className="CollectionSelector"><div className="CollectionSelector-loading">Подождите, идет загрузка коллекций...</div></div>;
        }
        return (
            <div className="CollectionSelector">
                <div className="CollectionSelector-surfaces">
                    <div className="CollectionSelector-surface">
                        <CollectionSurface
                            type={SurfaceTypes.WALLS}
                            selectTitle="Выберите коллекцию для стен"
                            selectTitleTooltip="Подсказка про выбор коллекции для стен"
                            enableTitle="Мне нужны стены"
                            enableTooltip="Подсказка про вкл/выкл стен"
                            optionsTitle="Настройки раскладки стен"
                            preview={true} />
                    </div>
                    <div className="CollectionSelector-surface">
                        <CollectionSurface
                            type={SurfaceTypes.FLOOR}
                            selectTitle="Выберите коллекцию для пола"
                            selectTitleTooltip="Подсказка про выбор коллекции для пола"
                            enableTitle="Мне нужен пол"
                            enableTooltip="Подсказка про вкл/выкл пола"
                            optionsTitle="Настройки раскладки пола"
                            preview={this.state.floorNeedPreview} />
                    </div>
                </div>
                <div className="CollectionSelector-actionsBar ActionsBar">
                    <Button label="Перейти к раскладке плитки" inline={true} type="primary" onClick={this.handleConfirmButton} />
                </div>
            </div>
        );
    }
}

export default CollectionSelector;
