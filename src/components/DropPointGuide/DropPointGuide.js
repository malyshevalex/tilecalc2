import React from 'react';
import PureComponent from 'react-pure-render/component';
import classNames from 'classnames';

import './DropPointGuide.styl';

class SVGDropPointGuide extends PureComponent {

    static propTypes = {
        ratio: React.PropTypes.number,
        size: React.PropTypes.number
    };

    static defaultProps = {
        ratio: 1,
        size: 6
    };

    constructor() {
        super();
        this.state = {
            visible: false,
            x: 0,
            y: 0
        };
    }

    render() {
        let { ratio, size } = this.props;
        let x = this.state.x * ratio;
        let y = this.state.y * ratio;
        let d = ['M', -size, -size, 'L', size, size, 'M', size, -size, 'L', -size, size].join(' ');
        return (
            <path
                d={d}
                transform={'translate(' + x + ' ' + y + ')'}
                className={classNames('Guide', 'Guide--drop', { 'Guide--visible': this.state.visible })} />
        );
    }
}

export default SVGDropPointGuide;

