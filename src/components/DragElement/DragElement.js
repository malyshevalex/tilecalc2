
import React from 'react';  //  eslint-disable-line no-unused-vars
import classNames from 'classnames';
import PureComponent from 'react-pure-render/component';

import AppStore from '../../stores/AppStore';
import AppActions from '../../actions/AppActions';

import './DragElement.styl';

const REACT_ATTR_RE = /data\-react[\w\-]+=['"][^"']*['"]/g;
const OFF_STATE = {
    enabled: false,
    x: -999,
    y: -999,
    w: 0,
    h: 0,
    markup: ''
};


class DragElement extends PureComponent {

    static propTypes = {
        alignment: React.PropTypes.string
    };

    static defaultProps = {
        alignment: 'center'
    };

    constructor() {
        super();
        this.state = {};
        Object.assign(this.state, OFF_STATE);
    }

    componentDidMount() {
        AppStore.on('drag.start', this.handleStart, this);
        AppStore.on('drag.stop', this.handleStop, this);
    }


    componentWillUnmount() {
        AppStore.off('drag.start', this.handleStart, this);
        AppStore.off('drag.stop', this.handleStop, this);
        AppStore.off('drag.move', this.handleMove, this);
    }

    getInitialMetrics(drag) {
        let x, y, dx, dy, w, h;
        w = drag.node.clientWidth;
        h = drag.node.clientHeight;
        switch (this.props.alignment) {
            case 'center':
                dx = -w / 2;
                dy = -h / 2;
                break;
            default:
                dx = 0;
                dy = 0;
        }
        x = drag.originX + dx;
        y = drag.originY + dy;
        return { x, y, dx, dy, w, h };
    }

    handleStart() {
        let drag = AppStore.getDrag();
        let metrics = this.getInitialMetrics(drag);
        this.areas = drag.areas;
        this.setState({
            enabled: true,
            markup: drag.node.outerHTML.replace(REACT_ATTR_RE, ''),
            ...metrics
        });
        AppStore.on('drag.move', this.handleMove, this);
    }

    handleStop() {
        AppStore.off('drag.move', this.handleMove, this);
        delete this.areas;
        this.setState(OFF_STATE);
    }

    handleMove(x, y) {
        x += this.state.dx;
        y += this.state.dy;
        this.setState({ x, y} );
        AppActions.checkDragIntersections([[x, y], [x + this.state.w, y], [x + this.state.w, y + this.state.h], [x, y + this.state.h]]);
    }

    render() {
        let style = {
            left: this.state.x,
            top: this.state.y,
            width: this.state.w,
            height: this.state.h
        };
        let cls = classNames('DragElement', {
            'DragElement--visible': this.state.enabled
        });
        return (
            <div className={cls} style={style} dangerouslySetInnerHTML={{__html: this.state.markup}}></div>
        );
    }
}

export default DragElement;
