import React from 'react';  //  eslint-disable-line no-unused-vars
import PureComponent from 'react-pure-render/component';

import float2str from '../../utils/float2str';
import ProjectStore from '../../stores/ProjectStore';

import './RoomArea.styl';

class RoomArea extends PureComponent {

    constructor() {
        super();
        this.state = {
            value: ProjectStore.getArea()
        };
    }

    componentDidMount() {
        ProjectStore.on('point.change', this.handleChange, this);
        ProjectStore.on('change', this.handleChange, this);
    }


    componentWillUnmount() {
        ProjectStore.off('point.change', this.handleChange, this);
        ProjectStore.off('change', this.handleChange, this);
    }

    handleChange() {
        this.setState({
            value: ProjectStore.getArea()
        });
    }

    render() {
        return (
            <div className="RoomArea">
                <span className="RoomArea-label">{this.props.label}</span>
                <span className="RoomArea-value">{float2str(this.state.value)}</span>
                <span className="RoomArea-unit">м²</span>
            </div>
        );
    }
}

export default RoomArea;
