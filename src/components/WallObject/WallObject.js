import React from 'react';
import classNames from 'classnames';

import ProjectStore from '../../stores/ProjectStore';
import WallObject from '../../models/WallObject';

import AbstractProjectElement from '../AbstractProjectElement';

import './WallObject.styl';

const PropTypes = React.PropTypes;


class SVGWallObject extends AbstractProjectElement {
    static propTypes = {
        model: PropTypes.instanceOf(WallObject).isRequired,
        active: PropTypes.bool,
        ratio: PropTypes.number
    };

    static defaultProps = {
        active: false,
        ratio: 1
    };

    elementType = 'object';

    assignModel(object) {
        this.setState({
            left: object.left,
            top: object.top,
            width: object.width,
            height: object.height
        });
    }

    render() {
        let {model, ratio, active, ...props} = this.props;
        let x = this.state.left * ratio;
        let y = (ProjectStore.getHeight() - this.state.top) * ratio;
        let width = this.state.width * ratio;
        let height = this.state.height * ratio;

        let error = active && ProjectStore.isWallObjectIntersects(model);
        let int = [];
        if (model.isDoor) {
            int = (
                <g className="WallObject-int">
                    <path d={`M${0.04 * ratio} ${height} V${0.04 * ratio} h${width - 0.08 * ratio} V${height} `} />
                    <circle cx={0.1 * ratio} cy={height / 2} r={0.02 * ratio} />
                </g>
            );
        } else if (model.isWindow) {
            int = (
                <g className="WallObject-int">
                    <rect x={0.04 * ratio} y={0.04 * ratio} width={width - 0.08 * ratio} height={height - 0.08 * ratio} />
                    <path d={`M${width / 2} ${0.04 * ratio} v${height - 0.08 * ratio}`} />
                </g>
            );
        }
        return (
            <g transform={`translate(${x} ${y})`} {...props} className={classNames('WallObject', {'WallObject--active': active, 'WallObject--error': error})}>
                <rect x="0" y="0" width={width} height={height} />
                {int}
            </g>
        );
    }
}

export default SVGWallObject;
