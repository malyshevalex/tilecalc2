import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

import ProjectStore from '../../stores/ProjectStore';
import Wall from '../../models/Wall';

import AbstractProjectElement from '../AbstractProjectElement';

import './Wall.styl';

const PropTypes = React.PropTypes;

class SVGWall extends AbstractProjectElement {

    static propTypes = {
        model: PropTypes.instanceOf(Wall).isRequired,
        ratio: PropTypes.number,
        thickness: PropTypes.number
    };

    static defaultProps = {
        ratio: 1,
        thickness: 12
    };


    componentWillMount() {
        super.componentWillMount();
        this.getObjects(this.props.model);
    }

    componentWillReceiveProps(nextProps) {
        super.componentWillReceiveProps(nextProps);
        if (nextProps.model !== this.props.model) {
            this.getObjects(nextProps.model);
        }
    }


    elementType = 'wall';

    assignModel(wall) {
        this.setState({
            x: wall.x1,
            y: wall.y1,
            length: wall.exactLength,
            angle: wall.angle,
            leftAngle: wall.leftAngle,
            rightAngle: wall.rightAngle
        });
    }

    getObjects(wall) {
        this.objects = ProjectStore.getObjectsForWall(wall);
    }

    handleModelChange(model) {
        if (model === this.props.model) {
            this.assignModel(model);
        } else if (model === this.props.model.leftSibling || model === this.props.model.rightSibling) {
            this.assignModel(this.props.model);
        }
    }

    getTransformMatrix() {
        let angle = this.state.angle, ratio = this.props.ratio;
        let cosA = Math.cos(angle);
        let sinA = Math.sin(angle);
        return [
            cosA,
            sinA,
            -sinA,
            cosA,
            this.state.x * ratio,
            this.state.y * ratio
        ];
    }

    getBoundingClientPoly() {
        let rect = ReactDOM.findDOMNode(this.refs.line).getBoundingClientRect();
        let sin = Math.sin(this.state.angle), cos = Math.cos(this.state.angle);
        let t2 = this.props.thickness / 2;
        let sameSign = (sin >= 0 && cos > 0 || sin <= 0 && cos < 0);
        sin = Math.abs(sin);
        cos = Math.abs(cos);
        if (sameSign) {
            return [
                [ rect.left + sin * t2, rect.top - cos * t2 ],
                [ rect.right + sin * t2, rect.bottom - cos * t2 ],
                [ rect.right - sin * t2, rect.bottom + cos * t2 ],
                [ rect.left - sin * t2, rect.top + cos * t2 ]
            ];
        } else {
            return [
                [ rect.left - sin * t2, rect.bottom - cos * t2 ],
                [ rect.right - sin * t2, rect.top - cos * t2 ],
                [ rect.right + sin * t2, rect.top + cos * t2 ],
                [ rect.left + sin * t2, rect.bottom + cos * t2 ]
            ];
        }
    }

    render() {
        let {ratio, thickness, onObjectClick, ...props} = this.props;
        let transform = 'matrix(' + this.getTransformMatrix(ratio).join(',') + ')';
        let width = this.state.length * ratio;
        let dx1 = -(thickness) * Math.tan(this.state.leftAngle / 2);
        let dx2 = (thickness) * Math.tan(this.state.rightAngle / 2);
        let points = [
            dx1, -thickness,
            width + dx2, -thickness,
            width, 0,
            0, 0
        ];
        return (
            <g transform={transform} className={classNames('Wall', {'Wall--active': this.state.active})}>
                <g className="Wall-wall" {...props}>
                    <polygon points={points.join(',')} className="Wall-poly" />
                    <line x1={dx1 / 2} y1={-thickness / 2} x2={width + dx2 / 2} y2={-thickness / 2} className="Wall-line" ref="line" />
                </g>
                {this.objects.map((o, i) => {
                    let ex = [];
                    if (o.isDoor) {
                        ex = <path d={`M ${(o.left + 0.04) * ratio},0 h ${(o.width - 0.08) * ratio} a ${(o.width - 0.08) * ratio},${(o.width - 0.08) * ratio} 0 0 1 ${(-o.width + 0.08) * ratio},${(o.width - 0.08) * ratio} Z`} />;
                    } else if (o.isWindow) {
                        ex = <path d={`M${o.left * ratio} ${-thickness / 2} h${o.width * ratio}`} strokeWidth="3" />;
                    }
                    return <g className="Wall-object" key={i} onClick={onObjectClick ? onObjectClick.bind(undefined, o) : undefined}><rect x={o.left * ratio} y={-thickness} width={o.width * ratio} height={thickness} />{ex}</g>;
                })}
            </g>
        );
    }
}

export default SVGWall;
