import React from 'react';
import PureComponent from 'react-pure-render/component';
import classNames from 'classnames';

import ProjectStore from '../../stores/ProjectStore';
import ProjectActions from '../../actions/ProjectActions';

import CollectionSelect from '../CollectionSelect';
import CollectionPreview from '../CollectionPreview';
import CollectionSurfaceOptions from '../CollectionSurfaceOptions';

import './CollectionSurface.styl';

const PropTypes = React.PropTypes;

class CollectionSurface extends PureComponent {

    static propTypes = {
        type: PropTypes.string.isRequired,
        selectTitle: PropTypes.string.isRequired,
        selectTitleTooltip: PropTypes.string,
        enableTitle: PropTypes.string.isRequired,
        selectTooltip: PropTypes.string,
        optionsTitle: PropTypes.string.isRequired,
        preview: PropTypes.bool,
        onCollectionSelect: PropTypes.func
    };

    static defaultProps = {
        viewPreview: false
    };

    componentWillMount() {
        this.updateStateFromStore();
    }

    componentDidMount() {
        ProjectStore.on('surface.collectionChange', this.handleSurfaceChange, this);
    }

    componentWillUnmount() {
        ProjectStore.off('surface.collectionChange', this.handleSurfaceChange, this);
    }

    handleSurfaceChange(surfaceType) {
        if (this.props.type === surfaceType) {
            this.updateStateFromStore();
        }
    }

    updateStateFromStore() {
        let collection = ProjectStore.getCollection(this.props.type);
        let newState = {
            enabled: ProjectStore.needSurface(this.props.type),
            manufacturer: collection[0],
            collection: collection[1]
        };
        let options = ProjectStore.getSurfaceOptions(this.props.type);
        Object.assign(newState, options);
        this.setState(newState);
    }

    handleDropCollectionClick = () => {
        ProjectActions.setSurfaceCollection(this.props.type, this.state.manufacturer, null);
    };

    handleCollectionSelect = (manufacturer, collection) => {
        ProjectActions.setSurfaceCollection(this.props.type, manufacturer, collection);
    };

    handleCollectionChangeState = (state) => {
        ProjectActions.setSurfaceNeeded(this.props.type, state);
    };

    handleOptionChange = (name, value) => {
        ProjectActions.setSurfaceOption(this.props.type, {
            [name]: value
        });
    };

    render() {
        let collectionBlock;
        let selected = !!(this.state.enabled && this.state.manufacturer && this.state.collection);
        let withPreview  = this.props.preview && selected;
        if (withPreview) {
            collectionBlock = (
                <div className="CollectionSurface-preview">
                    <CollectionPreview type={this.props.type} manufacturer={this.state.manufacturer} collection={this.state.collection} />
                    <span className="CollectionSurface-cross" onClick={this.handleDropCollectionClick} />
                </div>
            );
        } else {
            collectionBlock = (
                <div className="CollectionSurface-selects">
                    <CollectionSelect
                        type={this.props.type}
                        title={this.props.selectTitle}
                        titleTooltip={this.props.selectTitleTooltip}
                        enableTitle={this.props.enableTitle}
                        enableTooltip={this.props.enableTooltip}
                        enabled={this.state.enabled}
                        manufacturer={this.state.manufacturer}
                        collection={this.state.collection}
                        onChangeState={this.handleCollectionChangeState}
                        onSelect={this.handleCollectionSelect} />
                </div>
            );
        }
        return (
            <div className={classNames('CollectionSurface', { 'CollectionSurface--selected': selected })}>
                <div className="CollectionSurface-collection">
                    {collectionBlock}
                </div>
                <div className={classNames('CollectionSurface-options', {'CollectionSurface-options--disabled': !selected})}>
                    <CollectionSurfaceOptions
                        enabled={selected}
                        title={this.props.optionsTitle}
                        type={this.props.type}
                        manufacturer={this.state.manufacturer}
                        collection={this.state.collection}
                        jointWidth={this.state.jointWidth}
                        direction={this.state.direction}
                        onChange={this.handleOptionChange} />
                </div>
            </div>
        );
    }
}

export default CollectionSurface;
