import React from 'react';
import PureComponent from 'react-pure-render/component';

import CollectionsStore from '../../stores/CollectionsStore';

import TooltipPoint from '../TooltipPoint';

import './CollectionPreview.styl';

const PropTypes = React.PropTypes;

class CollectionPreview extends PureComponent {

    static propTypes = {
        type: PropTypes.string.isRequired,
        manufacturer: PropTypes.string.isRequired,
        collection: PropTypes.string.isRequired
    };

    render() {
        let collection = CollectionsStore.getCollection(this.props.type, this.props.manufacturer, this.props.collection);
        let manufacturer = CollectionsStore.getManufacturer(this.props.manufacturer);
        let country = CollectionsStore.getCountry(manufacturer.country);
        let type = CollectionsStore.getType(collection.type);
        return (
            <div className="CollectionPreview">
                <div className="CollectionPreview-img" style={{backgroundImage: `url(${collection.imgUrl})`}} ></div>
                <div className="CollectionPreview-props">
                    <div className="CollectionPreview-prop">Коллекция</div>
                    <div className="CollectionPreview-val CollectionPreview-val--strong">{collection.name}</div>
                    <div className="CollectionPreview-prop">Фабрика</div>
                    <div className="CollectionPreview-val">
                        <a href={manufacturer.url} target="_blank">{manufacturer.name}</a>
                        {' '}<TooltipPoint tooltip="Здесь будет будет подсказка про фабрику"/>{' '}
                        <span className="CollectionPreview-flag" style={{backgroundImage: `url(${country.flagUrl})`}} title={country.title} />
                    </div>
                    <div className="CollectionPreview-prop">Тип</div>
                    <div className="CollectionPreview-val"><a href={type.url} target="_blank">{type.title}</a></div>
                    <div className="CollectionPreview-prop">Цена от</div>
                    <div className="CollectionPreview-val">{collection.minPrice} руб/м² <TooltipPoint tooltip="Здесь будет подсказка про цену" /></div>
                </div>
            </div>
        );
    }
}

export default CollectionPreview;
