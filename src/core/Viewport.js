import EventEmitter from 'eventemitter3';

let initialized = false;
let viewport = {width: 0, height: 0};
const RESIZE_EVENT = 'resize';


function handleWindowResize() {
    if (viewport.width !== window.innerWidth || viewport.height !== window.innerHeight) {
        viewport = {width: window.innerWidth, height: window.innerHeight};
        Viewport.emit(RESIZE_EVENT, viewport);
    }
}

let Viewport = Object.assign({}, EventEmitter.prototype, {
    initialize() {
        if (!initialized) {
            initialized = true;
            window.addEventListener('resize', handleWindowResize);
            window.addEventListener('orientationchange', handleWindowResize);
        }
    },
    destroy() {
        if (initialized) {
            this.removeAllListeners(RESIZE_EVENT);
            window.removeEventListener('resize', handleWindowResize);
            window.removeEventListener('orientationchange', handleWindowResize);
        }
    },
    onResize(fn, context) {
        this.on(RESIZE_EVENT, fn, context);
    },
    off(fn, context) {
        this.removeListener(RESIZE_EVENT, fn, context);
    }
});

export default Viewport;
