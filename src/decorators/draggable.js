import React, { Component, PropTypes } from 'react';  //  eslint-disable-line no-unused-vars

const CLICK_DISTANCE = 3;

class DraggableComponent extends Component {

    static propTypes = {
        onDragStart: PropTypes.func,
        onDragStop: PropTypes.func,
        onDrag: PropTypes.func,
        onClick: PropTypes.func
    };

    dragStart(e) {
        e.preventDefault();
        let isTouch = !!(e.touches);
        if (isTouch) {
            e = e.touches[0];
        }
        this.dragState = {
            isTouch,
            startX: e.pageX,
            startY: e.pageY,
            maybeClick: true
        };
        if (this.props.onDragStart) {
            this.props.onDragStart.call(undefined, e);
        }
        if (isTouch) {
            document.addEventListener('touchmove', this.handleDrag);
            document.addEventListener('touchend', this.handleDragStop);
            document.addEventListener('touchcancel', this.handleDragStop);
        } else {
            document.addEventListener('mousemove', this.handleDrag);
            document.addEventListener('mouseup', this.handleDragStop);
        }
    }

    dragStop(e) {
        if (this.dragState) {
            if (this.dragState.isTouch) {
                document.removeEventListener('touchmove', this.handleDrag);
                document.removeEventListener('touchend', this.handleDragStop);
                document.removeEventListener('touchcancel', this.handleDragStop);
            } else {
                document.removeEventListener('mousemove', this.handleDrag);
                document.removeEventListener('mouseup', this.handleDragStop);
            }
            if (this.dragState.maybeClick) {
                if (this.props.onClick) {
                    this.props.onClick.call(undefined, e);
                }
            }
            if (this.props.onDragStop) {
                this.props.onDragStop.call(undefined, e);
            }
        }
    }

    drag(e) {
        if (this.dragState) {
            if (this.dragState.isTouch) {
                e = e.touches[0];
            }
            if (Math.abs(e.pageX - this.dragState.startX) > CLICK_DISTANCE || Math.abs(e.pageY - this.dragState.startY) > CLICK_DISTANCE) {
                this.dragState.maybeClick = false;
            }
            if (this.props.onDrag) {
                this.props.onDrag.call(undefined, e);
            }
        }
    }

    handleDragStart = (e) => this.dragStart(e);

    handleDragStop = (e) => this.dragStop(e);

    handleDrag = (e) => this.drag(e);

}


function draggable(ComposedComponent) {

    return class extends DraggableComponent {
        render() {
            let {onClick, onDragStart, onDragStop, onDrag, ...props} = this.props;
            if (onClick || onDrag || onDragStop || onDragStart) {
                props.onMouseDown = this.handleDragStart;
                props.onTouchStart = this.handleDragStart;
            }
            return (
                <ComposedComponent {...props} />
            );
        }
    };
}

export default draggable;
