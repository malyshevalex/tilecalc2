import React from 'react';
import ReactDOM from 'react-dom';
import pureComponentUpdate from 'react-pure-render/function';
import position from '../utils/position';
import Viewport from '../core/Viewport';

const PropTypes = React.PropTypes;

class AbstractPopup extends React.Component {

    static propTypes = {
        x: PropTypes.number,
        y: PropTypes.number,
        fromMouse: PropTypes.bool,
        alignment: PropTypes.string,
        margin: PropTypes.number,
        onClose: PropTypes.func.isRequired
    };

    static defaultProps = {
        fromMouse: false,
        alignment: 'C',
        margin: 12,
        onClose: function() {}
    };

    componentClass = null;

    shouldComponentUpdate = pureComponentUpdate;


    constructor(props) {
        super(props);
        this.state = {
            x: -9999,
            y: -9999
        };
        this.needPlace = false;
    }

    componentWillMount() {
        let props = {};
        Object.keys(this.props).forEach(key => {
            if (!AbstractPopup.propTypes[key]) {
                props[key] = this.props[key];
            }
        });
        props.close = this.close;
        props.place = this.place.bind(this);
        this.content = React.createElement(this.componentClass, props, this.props.children);
    }

    componentDidMount() {
        Viewport.onResize(this.place, this);
        this.place();
    }

    componentWillUnmount() {
        Viewport.off(this.place, this);
    }

    componentWillReceiveProps() {
        this.needPlace = true;
    }

    componentDidUpdate() {
        if (this.needPlace) {
            this.place();
        }
    }

    place() {
        let node = ReactDOM.findDOMNode(this),
            w = node.offsetWidth,
            h = node.offsetHeight,
            m = this.props.margin,
            al = this.props.alignment,
            parent = node.offsetParent,
            pWidth = parent.clientWidth,
            pHeight = parent.clientHeight,
            ox = 0,
            oy = 0;

        if (this.props.anchor) {
            let rect = ReactDOM.findDOMNode(this.props.anchor).getBoundingClientRect();
            ox = rect.left + rect.width / 2;
            oy = rect.top + rect.height / 2;
        } else {
            ox = this.props.x;
            oy = this.props.y;
        }

        if (this.props.fromMouse || this.props.anchor) {
            let pos = position.global(parent);
            ox -= pos.x;
            oy -= pos.y;
        }

        let x = Math.max(Math.min(ox - w / 2, pWidth - m - w), m);
        let y = Math.max(Math.min(oy - h / 2, pHeight - m - h), m);
        Array.prototype.forEach.call(al, function(a) {
            switch (a.toLowerCase()) {
                case 'r':
                    x = Math.max(ox - w - m, m);
                    break;
                case 'l':
                    x = Math.min(ox + m, pWidth - m - w);
                    break;
                case 't':
                    y = Math.min(oy + m, pHeight - m - h);
                    break;
                case 'b':
                    y = Math.max(oy - h - m, m);
            }
        });
        this.needPlace = false;
        this.setState({x, y});
    }

    handleMouseDown = (e) => {
        e.stopPropagation();
    };

    close = () => {
        this.props.onClose();
    };

    render() {
        let style = {
            left: this.state.x,
            top: this.state.y
        };
        return (
            <div className="Popup" style={style} onMouseDown={this.handleMouseDown}>
                <div className="Popup-close" onClick={this.close}>&times;</div>
                {this.content}
            </div>
        );
    }
}

function popup(ComposedComponent) {
    return class extends AbstractPopup {
        constructor(props) {
            super(props);
            this.componentClass = ComposedComponent;
        }
    };
}

export default popup;
