
import EventEmitter from 'eventemitter3';
import request from 'superagent';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';
import SurfaceTypes from '../constants/SurfaceTypes';
import SurfaceDirectionTypes from '../constants/SurfaceDirectionTypes';

import fixtures from '../settings.json';

const JOINT_WIDTHS = [1.5, 2, 2.5, 3, 3.5, 4];

let isReady = false;
let manufacturers = [];
let countries = {};
let types = {};


let CollectionsStore = Object.assign({}, EventEmitter.prototype, {
    isReady: function() {
        return isReady;
    },
    getCollectionsUrl: function() {
        return fixtures.collectionsUrl;
    },
    getWallsManufacturers: function() {
        return manufacturers.filter(m => m.wallsCollections && m.wallsCollections.length);
    },
    getFloorManufacturers: function() {
        return manufacturers.filter(m => m.floorCollections && m.floorCollections.length);
    },
    getManufacturers: function(type) {
        switch (type) {
            case SurfaceTypes.WALLS:
                return this.getWallsManufacturers();
            case SurfaceTypes.FLOOR:
                return this.getFloorManufacturers();
            default:
                return [];
        }
    },
    getManufacturer: function(manufacturer) {
        return manufacturers.find(m => m.id === manufacturer);
    },
    getCollections: function(type, manufacturer) {
        let man = this.getManufacturer(manufacturer);
        if (man) {
            switch (type) {
                case SurfaceTypes.WALLS:
                    return man.wallsCollections;
                case SurfaceTypes.FLOOR:
                    return man.floorCollections;
                default:
                    return [];
            }
        }
        return [];
    },
    getCollection: function(type, manufacturer, collection) {
        return this.getCollections(type, manufacturer).find(c => c.id === collection);
    },
    getCountry: function(country) {
        return countries[country];
    },
    getType: function(type) {
        return types[type];
    },
    getAvailableJointWidths: function() {
        return JOINT_WIDTHS;
    },
    getAvailableDirectionTypes: function(collection) {
        let types = [];
        if (collection) {
            if (collection.hasRect) {
                types.push(SurfaceDirectionTypes.HORIZONTAL, SurfaceDirectionTypes.VERTICAL);
            }
            if (collection.hasSquare) {
                types.push(SurfaceDirectionTypes.SQUARE, SurfaceDirectionTypes.DIAGONAL);
            }
        }
        return types;
    }
});

CollectionsStore.dispatchToken = Dispatcher.register((action) => {
    switch (action.type) {
        case ActionTypes.FETCH_COLLECTIONS:
            fetchCollections();
            break;
    }
});

function fetchCollections() {
    request
        .get(CollectionsStore.getCollectionsUrl(), null, null)
        .set('Accept', 'application/json')
        .end((err, res) => {
            if (!err) {
                let body = res.body;
                manufacturers = body.manufacturers;
                countries = body.countries;
                types = body.types;
                isReady = true;
                CollectionsStore.emit('ready');
            } else {
                alert('Failed to load connections!');
            }
        });
}

export default CollectionsStore;
