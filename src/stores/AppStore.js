
import EventEmitter from 'eventemitter3';
import delayed from 'delayed';

import area from '../utils/area';
import clip from '../utils/clip';

import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';
import ViewTypes from '../constants/ViewTypes';

let loading = false;
let projects = [];
let currentView = ViewTypes.WELCOME;
let tooltip = null;
let drag = null;


let AppStore = Object.assign({}, EventEmitter.prototype, {
    isLoading() {
        return loading;
    },

    getTooltip() {
        return tooltip;
    },

    getDrag() {
        return drag;
    },

    getProjects() {
        return projects;
    },

    getCurrentView() {
        return currentView;
    }
});

function changeView(view) {
    currentView = view;
    AppStore.emit('change');
}

AppStore.dispatchToken = Dispatcher.register((action) => {
    switch (action.type) {
        case ActionTypes.SHOW_TOOLTIP:
            showTooltip(action.x, action.y, action.text);
            break;
        case ActionTypes.HIDE_TOOLTIP:
            hideTooltip();
            break;
        case ActionTypes.START_DRAG:
            startDrag(action.node, action.event, action.areas);
            break;
        case ActionTypes.CHECK_DRAG_INTERSECTIONS:
            checkDragIntersections(action.poly);
            break;
        case ActionTypes.CREATE_PROJECT:
            delayed.defer(() => changeView(ViewTypes.ROOM_EDITOR));
            break;
        case ActionTypes.CHANGE_VIEW:
            changeView(action.view);
            break;
    }
});

function showTooltip(x, y, text) {
    tooltip = {
        x, y, text
    };
    AppStore.emit('tooltip.show');
}

function hideTooltip() {
    tooltip = null;
    AppStore.emit('tooltip.hide');
}

function startDrag(node, event, areas) {
    let isTouch = false;
    if (event.touches) {
        isTouch = true;
        event = event.touches[0];
    }
    let ox = event.pageX, oy = event.pageY;
    drag = {
        node,
        areas,
        originX: ox,
        originY: oy
    };

    AppStore.emit('drag.start');

    function handleMove(e) {
        e = e.touches ? e.touches[0] : e;
        AppStore.emit('drag.move', e.pageX, e.pageY);
    }

    function handleStop() {
        if (isTouch) {
            document.removeEventListener('touchmove', handleMove);
            document.removeEventListener('touchend', handleStop);
            document.removeEventListener('touchcancel', handleStop);
        } else {
            document.removeEventListener('mousemove', handleMove);
            document.removeEventListener('mouseup', handleStop);
        }
        AppStore.emit('drag.stop', drag.area);
    }
    if (isTouch) {
        document.addEventListener('touchmove', handleMove);
        document.addEventListener('touchend', handleStop);
        document.addEventListener('touchcancel', handleStop);
    } else {
        document.addEventListener('mousemove', handleMove);
        document.addEventListener('mouseup', handleStop);
    }
}

function checkDragIntersections(poly) {
    let selectedAreaName, selectedArea = 0;
    console.log(drag.areas);
    for (let areaName in drag.areas) {
        if (drag.areas.hasOwnProperty(areaName)) {
            let intersection = clip(drag.areas[areaName], poly);
            if (intersection.length) {
                let cArea = area(intersection);
                if (cArea > selectedArea) {
                    selectedArea = cArea;
                    selectedAreaName = areaName;
                }
            }
            selectedArea++;
        }
    }
    if (drag.area && selectedAreaName !== drag.area) {
        AppStore.emit('drag.leave', drag.area);
    }
    if (selectedAreaName && selectedAreaName !== drag.area) {
        AppStore.emit('drag.enter', selectedAreaName);
    }
    drag.area = selectedAreaName;
}

export default AppStore;
