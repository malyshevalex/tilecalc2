
import EventEmitter from 'eventemitter3';
import Dispatcher from '../core/Dispatcher';
import ActionTypes from '../constants/ActionTypes';
import Point from '../models/Point';
import Wall from '../models/Wall';
import WallObject from '../models/WallObject';
import Bounds from '../models/Bounds';
import cm from '../utils/cm';
import area from '../utils/area';
import rectIntersection from '../utils/rectIntersection.js';

import CollectionsStore from './CollectionsStore';
import SurfaceTypes from '../constants/SurfaceTypes';

let HEIGHT_CONSTRAINT = [2, 3.5];

let data = {
    name: '',
    wallsHead: null,
    height: 2.8,
    surfaces: {
        [SurfaceTypes.WALLS]: {
            needed: true,
            collection: null,
            options: {
                jointWidth: 3,
                direction: null
            }
        },
        [SurfaceTypes.FLOOR]: {
            needed: true,
            collection: null,
            options: {
                jointWidth: 3,
                direction: null
            }
        }
    }
};

let ProjectStore = Object.assign({}, EventEmitter.prototype, {
    getName() {
        return data.name;
    },
    getHeightConstraints() {
        return HEIGHT_CONSTRAINT;
    },
    getHeight() {
        return data.height;
    },
    getWallsHead() {
        return data.wallsHead;
    },
    cloneScene() {
        let points = this.getPoints().map(p => p.clone());
        let walls = constructWalls(points);
        return { points, walls };
    },
    getArea() {
        return cm.round(area(Wall.wallsToPoints(data.wallsHead)));
    },
    getObjectsForWall(wall) {
        return wall.objects;
    },
    isWallObjectIntersects(object) {
        return data.objects.filter(o => (o.wallIndex === object.wallIndex && object !== o)).some(o => o.isIntersectWith(object));
    },
    needSurface: function(type) {
        return data.surfaces[type] && data.surfaces[type].needed;
    },
    getCollection: function(type) {
        if (data.surfaces[type] && data.surfaces[type].collection) {
            return data.surfaces[type].collection.split('/');
        }
        return [];
    },
    getSurfaceOptions: function(type) {
        let surface = data.surfaces[type];
        return surface ? surface.options : {};
    }
});

ProjectStore.dispatchToken = Dispatcher.register((action) => {
    switch (action.type) {
        case ActionTypes.CREATE_PROJECT:
            createProject(action.points);
            break;
        case ActionTypes.MOVE_POINT:
            movePoint(action.index, action.dx, action.dy);
            break;
        case ActionTypes.DROP_POINT:
            dropPoint(action.wall);
            break;
        case ActionTypes.INSERT_POINT:
            insertPoint(action.wall, action.x, action.y);
            break;
        case ActionTypes.RESIZE_WALL:
            resizeWall(action.wall, action.length);
            break;
        case ActionTypes.ROTATE_SCENE:
            rotateScene();
            break;
        case ActionTypes.MIRROR_SCENE:
            mirrorScene();
            break;
        case ActionTypes.SET_HEIGHT:
            setHeight(action.height);
            break;
        case ActionTypes.ADD_OBJECT_TO_WALL:
            addObjectToWall(action.wall, action.objectType);
            break;
        case ActionTypes.SAVE_WALL_OBJECT:
            saveWallObject(action.object);
            break;
        case ActionTypes.DELETE_WALL_OBJECT:
            deleteWallObject(action.object);
            break;
        case ActionTypes.MOVE_WALL_OBJECT:
            moveWallObject(action.object, action.left, action.bottom);
            break;
        case ActionTypes.RESIZE_WALL_OBJECT:
            resizeWallObject(action.object, action.width, action.height);
            break;
        case ActionTypes.EDIT_WALL_OBJECT:
            editWallObject(action.object);
            break;
        case ActionTypes.SET_SURFACE_NEEDED:
            setSurfaceNeeded(action.surfaceType, action.state);
            break;
        case ActionTypes.SET_SURFACE_COLLECTION:
            setSurfaceCollection(action.surfaceType, action.manufacturer, action.collection);
            break;
        case ActionTypes.SET_SURFACE_OPTIONS:
            setSurfaceOptions(action.surfaceType, action.options);
            break;
    }
});

function constructRoom(points) {
    if (points.length < 3) {
        throw new Error('points array length should be greater then 3');
    }
    return points.reduceRight((prev, point) => new Wall(point, prev), null).close();
}

function isWallIntersects(wall) {
    let right = wall.rightSibling,
        left = wall.leftSibling;
    if (right && left) {
        let test = right.rightSibling;
        while (test && test !== left) {
            if (wall.isIntersectsWith(test)) {
                return true;
            }
            test = test.rightSibling;
        }
    }
    return false;
}

/**
 * Validate points and walls
 * @param {Array} points
 * @param {Array} walls
 * @param {Array} [objects]
 * @returns {boolean}
 */
function validate(points, walls, objects) {
    return (points.every(p => p.isValid)
        && walls.every(w => w.isValid && !isWallIntersects(w))
        && (!objects || !objects.length || objects.every(o => (o.right <= walls[o.wallIndex].length))));
}

function assignHead(head) {
    data.wallsHead = head;
    ProjectStore.emit('room.change');
}

function createProject(points) {
    //data.name = 'Новый проект';
    assignHead(constructRoom(points));
}

/**
 * Move point
 * @param {Number} index
 * @param {Number} x
 * @param {Number} y
 */
function movePoint(index, x, y) {
    let point = data.points[index];
    let old = {
        x: point.x,
        y: point.y
    };
    point.moveTo(x, y);
    if (validate(data.points, data.walls, data.objects)) {
        ProjectStore.emit('point.change', point);
        ProjectStore.emit('wall.change', point.leftWall);
        ProjectStore.emit('wall.change', point.rightWall);
    } else {
        point.moveTo(old.x, old.y);
    }
}

function dropPoint(wall) {
    assignHead(Wall.dropWall(wall));
}

function insertPoint(wall, x, y) {
    assignHead(Wall.insertPoint(wall, x, y));
}

function resizeWall(wall, length) {
    assignHead(Wall.resizeWall(wall, length));
}

function rotateScene() {
    let newHead = Wall.transformMatrix(data.wallsHead, [0, 1, -1, 0, 0, 0], { inPlace: true });
    assignHead(newHead);
}

function mirrorScene() {
    let newHead = Wall.transformMatrix(data.wallsHead, [-1, 0, 0, 1, 0, 0], { reverse: true });
    assignHead(newHead);
}

function setHeight(height) {
    if (height >= HEIGHT_CONSTRAINT[0] && height <= HEIGHT_CONSTRAINT[1]) {
        data.height = height;
    }
}

function addObjectToWall(wall, objectType) {
    let isDoor = objectType === WallObject.DOOR;
    let wallHeight = data.height;
    let wallWidth = wall.length;
    let objects = ProjectStore.getWallObjects(index);

    function getAvailablePlaces(i, place) {
        let places = [];
        let object = objects[i];
        place = {
            ...place,
            right: place.left + place.width,
            top: place.bottom + place.height
        };

        if (object && rectIntersection(place, object)) {
            if (object.left > place.left && object.left < place.right) {
                places.push({
                    ...place,
                    width: object.left - place.left
                });
            }
            if (object.bottom > place.bottom && object.bottom < place.top) {
                places.push({
                    ...place,
                    height: object.bottom - place.bottom
                });
            }
            if (object.right > place.left && object.right < place.right) {
                places.push({
                    ...place,
                    left: object.right,
                    width: place.right - object.right
                });
            }
            if (object.top > place.bottom && object.top < place.top) {
                places.push({
                    ...place,
                    bottom: object.top,
                    height: place.top - object.top
                });
            }
        } else {
            places.push(place);
        }
        if (i + 1 < objects.length) {
            return places.reduce((res, p) => res.concat(getAvailablePlaces(i + 1, p)), []);
        } else {
            return places;
        }
    }

    let minSize = WallObject.getMinSize(objectType);

    // get all available places for minimal object
    let avPlaces = getAvailablePlaces(0, {
        left: 0,
        bottom: 0,
        width: wallWidth,
        height: wallHeight
    }).filter(p => (p.width >= minSize.width && p.height >= minSize.height && (!isDoor || p.bottom === 0)));
    // sort places by area reversed
    avPlaces.sort((a, b) => {
        let aArea = a.width * a.height, bArea = b.width * b.height;
        if (aArea > bArea) {
            return -1;
        }
        if (aArea < bArea) {
            return 1;
        }
        return 0;
    });

    // place object center in first place (largest)
    let selectedPlace = avPlaces[0];
    if (selectedPlace) {
        let object = new WallObject(objectType, index, {
            ...minSize,
            left: cm.round(selectedPlace.left + (selectedPlace.width - minSize.width) / 2),
            bottom: cm.round(isDoor ? 0 : selectedPlace.bottom + (selectedPlace.height - minSize.height) / 2)
        });
        data.objects.push(object);
        ProjectStore.emit('object.edit', object);
    }
}

function editWallObject(object) {
    ProjectStore.emit('object.edit', object);
}

function saveWallObject(object) {
    ProjectStore.emit('object.save', object);
}

function deleteWallObject(object) {
    let index = data.objects.indexOf(object);
    data.objects.splice(index, 1);
    ProjectStore.emit('object.delete', object);
}

function moveWallObject(object, left, bottom) {
    let wallLength = ProjectStore.getWall(object.wallIndex).length,
        wallHeight = ProjectStore.getHeight();
    object.move(cm.round(Math.min(left, wallLength - object.width)), cm.round(Math.min(bottom, wallHeight - object.height)));
    ProjectStore.emit('object.change', object);
}

function resizeWallObject(object, width, height) {
    let wallLength = ProjectStore.getWall(object.wallIndex).length,
        wallHeight = ProjectStore.getHeight();
    object.resize(cm.round(Math.min(width, wallLength - object.left)), cm.round(Math.min(height, wallHeight - object.bottom)));
    ProjectStore.emit('object.change', object);
}

function setSurfaceNeeded(type, state) {
    let surface = data.surfaces[type];
    if (surface) {
        surface.needed = !!state;
        ProjectStore.emit('surface.collectionChange', type);
    }
}

function setSurfaceCollection(type, manufacturer, collection) {
    let surface = data.surfaces[type];
    if (surface) {
        let collectionData = null;
        if (manufacturer && collection) {
            surface.collection = [manufacturer, collection].join('/');
            collectionData = CollectionsStore.getCollection(type, manufacturer, collection);
        } else if (manufacturer) {
            surface.collection = [manufacturer, ''].join('/');
        } else {
            surface.collection = null;
        }
        if (collectionData) {
            let directionTypes = CollectionsStore.getAvailableDirectionTypes(collectionData);
            if (directionTypes.indexOf(ProjectStore.getSurfaceOptions(type).direction) == -1) {
                setSurfaceOptions(type, {
                    direction: directionTypes[0]
                });
            }
        }
        ProjectStore.emit('surface.collectionChange', type);
        if (type === SurfaceTypes.WALLS) {
            if (manufacturer && collection && CollectionsStore.getCollection(SurfaceTypes.FLOOR, manufacturer, collection)) {
                setSurfaceCollection(SurfaceTypes.FLOOR, manufacturer, collection);
            } else {
                setSurfaceCollection(SurfaceTypes.FLOOR, null, null);
            }
        } else if (type === SurfaceTypes.FLOOR && !ProjectStore.getCollection(SurfaceTypes.WALLS)[1]) {
            if (manufacturer && collection && CollectionsStore.getCollection(SurfaceTypes.WALLS, manufacturer, collection)) {
                setSurfaceCollection(SurfaceTypes.WALLS, manufacturer, collection);
            }
        }
    }
}

function setSurfaceOptions(type, options) {
    let surface = data.surfaces[type];
    if (surface) {
        surface.options = Object.assign({}, surface.options, options);
        ProjectStore.emit('surface.optionsChange', type);
    }
}

export default ProjectStore;
