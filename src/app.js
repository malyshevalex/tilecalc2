import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';

import Viewport from './core/Viewport';
import AppStore from './stores/AppStore';  //  eslint-disable-line no-unused-vars
import ProjectStore from './stores/ProjectStore';  //  eslint-disable-line no-unused-vars
import CollectionsStore from './stores/CollectionsStore';  //  eslint-disable-line no-unused-vars
import CollectionsActions from './actions/CollectionsActions';

import App from './components/App';


function disableContextMenu() {
    let body = window.document.querySelector('body');
    window.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    }, body);
}

function disableZoom() {
    let body = window.document.querySelector('body');
    window.addEventListener('mousewheel', function(e) {
        if (e.ctrlKey) {
            e.preventDefault();
        }
    }, body);
    body.style.zoom = 'reset';
}

function run() {
    disableContextMenu();
    disableZoom();
    CollectionsActions.fetchCollections();
    Viewport.initialize();
    ReactDOM.render(React.createElement(App), document.getElementById('app'));
}

Promise.all([
    new Promise(resolve => {
        window.addEventListener('DOMContentLoaded', resolve);
    })
]).then(run);
