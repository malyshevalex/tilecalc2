import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
    HORIZONTAL: null,
    VERTICAL: null,
    SQUARE: null,
    DIAGONAL: null
});
