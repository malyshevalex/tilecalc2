import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
    WELCOME: null,
    ROOM_EDITOR: null,
    COLLECTION_SELECTOR: null,
    LAYOUT_EDITOR: null
});
