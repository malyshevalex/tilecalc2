import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
    WALLS: null,
    FLOOR: null
});
