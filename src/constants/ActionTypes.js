import keyMirror from 'fbjs/lib/keyMirror';

export default keyMirror({
    FETCH_COLLECTIONS: null,
    SHOW_TOOLTIP: null,
    HIDE_TOOLTIP: null,
    START_DRAG: null,
    STOP_DRAG: null,
    CHECK_DRAG_INTERSECTIONS: null,
    CHANGE_VIEW: null,
    CREATE_PROJECT: null,
    MOVE_POINT: null,
    DROP_POINT: null,
    INSERT_POINT: null,
    RESIZE_WALL: null,
    ROTATE_SCENE: null,
    MIRROR_SCENE: null,
    SET_HEIGHT: null,
    ADD_OBJECT_TO_WALL: null,
    SAVE_WALL_OBJECT: null,
    DELETE_WALL_OBJECT: null,
    MOVE_WALL_OBJECT: null,
    RESIZE_WALL_OBJECT: null,
    EDIT_WALL_OBJECT: null,
    SET_SURFACE_NEEDED: null,
    SET_SURFACE_COLLECTION: null,
    SET_SURFACE_OPTIONS: null
});
