var fs = require('fs');
var webpack = require('webpack');
var path = require('path');
var argv = require('minimist')(process.argv.slice(2));

const PROJECT_NAME = 'tilecalc2';
const RELEASE = !!argv['release'];
const DEBUG = !RELEASE;
const DATA_URI_LIMIT = 10000000;

const version = JSON.parse(fs.readFileSync('package.json', 'utf8')).version;
const entry = ['./src/app.js'];

var plugins = [
    new webpack.optimize.OccurenceOrderPlugin()
];

if (RELEASE) {
    plugins = plugins.concat([
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.AggressiveMergingPlugin()
    ]);
}

var styleLoader = 'style',
    cssLoader = DEBUG ? 'css' : 'css?minimize';

function styleLoaders() {
    return [styleLoader, cssLoader, 'postcss'].concat(Array.prototype.slice.call(arguments));
}

function url(mimeType) {
    return 'url?limit=' + DATA_URI_LIMIT + '&name=[path][name].[ext]&mimetype=' + mimeType;
}

function getPath() {
    return RELEASE ? path.join(__dirname, 'dist') : path.join(__dirname, 'build');
}

function getFilename() {
    return RELEASE ? PROJECT_NAME + '.' + version + '.min.js' : 'bundle.js';
}

module.exports = {
    entry: entry,
    output: {
        path: getPath(),
        filename: getFilename()
    },
    cache: DEBUG,
    debug: DEBUG,
    devtool: DEBUG ? 'source-map': false,
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: ['babel-loader']
            },
            {
                test: /\.json$/,
                loaders: ['json-loader']
            },
            {
                test: /\.css$/,
                loaders: styleLoaders()
            },
            {
                test: /\.styl$/,
                loaders: styleLoaders('stylus')
            },
            {
                test: /\.png$/,
                loader: url('image/png')
            },
            {
                test: /\.jpg$/,
                loader: url('image/jpg')
            },
            {
                test: /\.svg$/,
                loader: url('image/svg+xml')
            },
            {
                test: /\.svg\.defs$/,
                loader: 'svg-defs-loader'
            }
        ]
    },
    plugins: plugins,
    postcss: [
        require('postcss-normalize'),
        require('autoprefixer')({ browsers: ['last 2 versions', 'IE >= 9'] })
    ]
};
